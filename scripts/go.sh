#!/usr/bin/env bash
# Idweaver Drupal installer
# The purpose of this script is to execute everything needed to start working on a Drupal website

cd /app/drupal

# Update drupal site to current code version
go_update() {

    composer install --no-suggest

    drush -y updatedb
    drush -y config:import
    drush -y entity:updates

    go_clear_cache
}

# Install a fresh Drupal from current code version
go_install() {
    if [[ ! -f web/sites/default/settings.php ]]; then
        cp web/sites/default/settings.example.php web/sites/default/settings.php
    fi

    composer install --no-suggest

    # change file chmod
    chmod 777 web/sites/default/settings.php

    # install drupal and import all existing configuration
    drush -y site-install minimal --site-name="Warcraft" --account-name=admin --account-pass=PQkk5YhDM4 --config-dir="../config/sync"

    # It seems we need to import again for config splits to be taken into account
    drush -y config:import

    go_clear_cache

    drush -y entity:updates

    go_clear_cache
}

# Clear cache
# and build necessary assets (grunt, gulp, whatever)
go_clear_cache() {

    drush -y cache:rebuild
}

# (re)load fixtures
go_fixtures_reload() {

    drush fixtures:reload all
}

# Helptext
display_usage() {
    echo ""
    echo "Available options are:"
    echo ""
    echo "-u|--update: Apply all necessary updates"
    echo "-i|--install: Install or reinstall from scratch (Warning: destructive operation, flushes all content!)"
    echo "-c|--cache: Clear all cache"
    echo "-f|--fixtures: Load or reload fixtures (Warning: destructive operation, replaces most content)"
    echo ""
}


# Variables defaults
start=$SECONDS
update=false
install=false
clearCache=false
reloadFixtures=false
forceYes=false

# Color Constants
SUCCESS='\033[1;32m' # Green
WARNING='\033[1;33m' # Orange
NC='\033[0m' # No color

# No arguments => display helptext
if [[ $# -eq 0 ]] ; then
    display_usage
    exit 0
fi

# Read all arguments and mark tasks to execute
while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        # Install environment
        -i|--install)
        install=true
        ;;
        # Update environment
        -u|--update)
        update=true
        ;;
        # Clear cache
        -c|--cache)
        clearCache=true
        ;;
        # Enable fixtures
        -f|--fixtures)
        reloadFixtures=true
        ;;
        # Helptext if option is invalid
        -h|--help)
        display_usage
        ;;
        # Error for unknown options
        *)
        echo "Unknown option '$key'. Use lando go -h for help"
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

# Tasks are always executed in the same order, whatever the given order of parameters

# Install
if $install ; then
    go_install

    echo ""
    echo -e "${SUCCESS}Installation done."
    echo -e "Go back to work: http://${LANDO_APP_NAME}.${LANDO_DOMAIN}${NC}"
fi

# Update
if $update ; then
    go_update

    echo ""
    echo -e "${SUCCESS}Update done."
    echo -e "Go back to work: http://${LANDO_APP_NAME}.${LANDO_DOMAIN}${NC}"
fi

# Clear cache
if $clearCache ; then
    go_clear_cache

    echo ""
    echo -e "${SUCCESS}Cache cleared.${NC}"
fi

# Reload fake content
if $reloadFixtures ; then
    go_fixtures_reload

    echo ""
    echo -e "${SUCCESS}Fixtures (re)loaded.${NC}"
fi

duration=$(( SECONDS - start ))

echo ""
echo -e "${SUCCESS}Done in ${duration} seconds.${NC}"
