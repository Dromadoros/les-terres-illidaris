<?php

namespace Drupal\warcraft_media\TwigExtension;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;

/**
 * Class customTwigExtension.
 *
 * @package Drupal\warcraft_media
 *
 * extend Drupal's Twig_Extension class
 */
class MediaUrl extends \Twig_Extension {

  /**
   * @var \Drupal\warcraft_media\Services\MediaProvider $mediaProvider
   */
  private $mediaProvider;

  /**
   * MediaUrl constructor.
   *
   * @param \Drupal\warcraft_media\Services\MediaProvider $mediaProvider
   */
  public function __construct($mediaProvider) {
    $this->mediaProvider = $mediaProvider;
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   */
  public function getName() {
    return 'warcraft_media.twig.mediaurl';
  }

  /**
   * Generates a list of all Twig functions that this extension defines.
   *
   * @return array|\Twig\TwigFunction[]
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('media_url', [$this, 'mediaUrl']),
    ];
  }

  /**
   * Get the Url of the Media.
   *
   * @param \Drupal\media\Entity\Media $media
   * @param string $style
   *
   * @return string|null
   */
  public function mediaUrl($media, $style = 'image_1920_1080'): ?string {
    if (!$media || !$media instanceof MediaInterface) {
      return NULL;
    }

    return $this->mediaProvider->getMediaUrl($media, $style);
  }

}
