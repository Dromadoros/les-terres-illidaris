<?php

namespace Drupal\warcraft_media\Services;

use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;

/**
 * Class MediaProvider
 *
 * @package Drupal\warcraft_media\Services
 */
class MediaProvider {
  const IMAGE_BUNDLE = 'image';
  const VIDEO_BUNDLE = 'video';
  const FILE_BUNDLE = 'file';
  const FIELD_MEDIA_IMAGE_NAME = 'field_media_image';
  const FIELD_MEDIA_VIDEO_NAME = 'field_media_video_file';
  const FIELD_MEDIA_FILE_NAME = 'field_media_file';

  /**
   * @param $media
   * @param $style
   *
   * @return bool|string
   */
  public function getMediaUrl(Media $media, $style = 'image_1920_1080') {
    $media_id = [];

    switch ($media->bundle()) {
      case self::VIDEO_BUNDLE:
        $media_id = $media->get(self::FIELD_MEDIA_VIDEO_NAME)->getValue();
        break;

      case self::IMAGE_BUNDLE:
        $media_id = $media->get(self::FIELD_MEDIA_IMAGE_NAME)->getValue();
        break;

      case self::FILE_BUNDLE:
        $media_id = $media->get(self::FIELD_MEDIA_FILE_NAME)->getValue();
        break;
    }

    $file = File::load($media_id[0]['target_id']);

    if (!$file) {
      return FALSE;
    }

    $uri = $file->getFileUri();

    if (self::IMAGE_BUNDLE !== $media->bundle()) {
      return file_url_transform_relative(file_create_url($uri));
    }

    $imageStyle = ImageStyle::load($style);

    return $imageStyle->buildUrl($uri);
  }
}
