<?php

namespace Drupal\warcraft_news\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a 'News List' Block.
 *
 * @Block(
 *   id = "news_list_block",
 *   admin_label = @Translation("News list block"),
 *   category = @Translation("Mangos"),
 * )
 */
class NewsListBlock extends BlockBase implements BlockPluginInterface {

  /**
   * @return array|void
   */
  public function build() {
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'news')
      ->condition('status', 1)
      ->sort('field_news_published_date', 'DESC')
      ->execute();

    if (!$nids) {
      return [];
    }

    $nodes = Node::loadMultiple($nids);
    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $nodesView = $viewBuilder->viewMultiple($nodes, 'teaser');

    return [
      '#theme' => 'list_news',
      '#news' => $nodesView,
    ];
  }

}
