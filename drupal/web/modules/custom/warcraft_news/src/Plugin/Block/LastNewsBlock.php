<?php

namespace Drupal\warcraft_news\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Subscription' Block.
 *
 * @Block(
 *   id = "last_news_block",
 *   admin_label = @Translation("Last news block"),
 *   category = @Translation("Mangos"),
 * )
 */
class LastNewsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $newsId = \Drupal::config('warcraft_core.settings')->get('news');
    $news = Node::load($newsId);

    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'news')
      ->condition('status', 1)
      ->sort('field_news_published_date', 'DESC')
      ->range(0, 3)
      ->execute();

    if (!$nids) {
      return [];
    }

    $nodes = Node::loadMultiple($nids);
    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $nodesView = $viewBuilder->viewMultiple($nodes, 'teaser');

    return [
      '#theme' => 'last_news',
      '#news' => $nodesView,
      '#newsUrl' => $news ? $news->toUrl()->toString() : '/news',
    ];
  }

}
