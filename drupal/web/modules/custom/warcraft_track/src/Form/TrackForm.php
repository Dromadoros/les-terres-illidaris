<?php

namespace Drupal\warcraft_track\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TrackForm.
 */
class TrackForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'track_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $currentNode */
    $currentNode = \Drupal::routeMatch()->getParameter('node');

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail', [], ['context' => 'warcraft_track']),
      '#required' => TRUE,
    ];

    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Track proof', [], ['context' => 'warcraft_track']),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t(
          'Maximum size: 5mb',
          [],
          ['context' => 'warcraft_track']
        ),
      ],
      '#required' => TRUE,
      '#size' => 5,
      '#upload_location' => 'public://track/proof/',
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
      ],
    ];
    $form['track_id'] = [
      '#type' => 'hidden',
      '#value' => $currentNode->id(),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_track']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $to = \Drupal::config('system.site')->get('mail');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');
    $result = $mailManager->mail('warcraft_track', 'proof', $to, $langcode, $values);

    if (!$result) {
      \Drupal::messenger()->addError($this->t('Message not sent correctly', [], ['context' => 'warcraft_core']));
      return;
    }

    \Drupal::messenger()->addMessage($this->t('Message sent with success', [], ['context' => 'warcraft_core']));
  }

}
