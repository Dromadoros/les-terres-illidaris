<?php

namespace Drupal\warcraft_track\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Provides a 'TracksBlock' block.
 *
 * @Block(
 *  id = "tracks_block",
 *  admin_label = @Translation("Tracks block"),
 *  category = @Translation("Mangos"),
 * )
 */
class TracksBlock extends BlockBase implements BlockPluginInterface {

  /**
   * @inheritDoc
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $tracks = \Drupal::entityQuery('node')
      ->condition('type', 'track')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_track_has_a_winner', 0)
      ->sort('field_track_published_date', 'DESC')
      ->execute();

    $oldTracks = \Drupal::entityQuery('node')
      ->condition('type', 'track')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_track_has_a_winner', 1)
      ->sort('field_track_published_date', 'DESC')
      ->execute();

    return [
      '#theme' => 'tracks_list',
      '#tracks' => Node::loadMultiple($tracks),
      '#oldTracks' => Node::loadMultiple($oldTracks),
    ];
  }

}
