<?php

namespace Drupal\warcraft_track\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\warcraft_track\Form\TrackForm;

/**
 * Provides a 'ProofTrackBlock' block.
 *
 * @Block(
 *  id = "proof_track_block",
 *  admin_label = @Translation("Proof Track block"),
 *  category = @Translation("Mangos"),
 * )
 */
class ProofTrackBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm(TrackForm::class);

    return $form;
  }

}
