<?php

namespace Drupal\warcraft_armory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\User;
use Drupal\warcraft_armory\Form\SearchCharacter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ArmoryController
 *
 * @package Drupal\warcraft_armory\Controller
 */
class ArmoryController extends ControllerBase {

  const ALLIANCE_RACES = [1, 3, 4, 7, 11];

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array
   */
  public function index(Request $request) {
    $characterName = $request->query->get('name', '');
    Database::setActiveConnection('character');
    $db = Database::getConnection();


    $onlines = $db
      ->query(
        'SELECT 
    *
FROM
    characters
WHERE
    characters.online = 1;'
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    $characters = $db
      ->query(
        'SELECT * FROM characters WHERE characters.name LIKE \'%' . $characterName . '%\' and characters.level > 58 and characters.level <= 70 and characters.name is not null and characters.account > 7'
      )
      ->fetchAll();

    /** @var User $user */
    $user = User::load(\Drupal::currentUser()->id());

    return [
      '#theme' => 'armory',
      '#characters' => $characters,
      '#onlines' => count($onlines),
      '#form' => \Drupal::formBuilder()->getForm(SearchCharacter::class),
      '#isAdmin' => $user ? ($user->hasRole('administrator') || (int) $user->id() === 1) : FALSE,
    ];
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array
   */
  public function detail(Request $request, $name = NULL) {
    if ($name === NULL) {
      throw new NotFoundHttpException();
    }

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $data = $db
      ->query(
        'SELECT 
    *
FROM
    characters characters
        LEFT JOIN
    character_inventory inventory ON characters.guid = inventory.guid  
WHERE
    characters.name = \'' . $name . '\'
        AND inventory.slot <= 18
        AND inventory.bag = 0;'
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (empty($data)) {
      throw new NotFoundHttpException();
    }

    $level = $data[0]['level'];
    /** @var \Drupal\warcraft_armory\Services\ItemInfo $itemService */
    $itemService = \Drupal::service('warcraft_armory.item_info');
    $items = [];
    foreach ($data as $item) {
      $itemData = $itemService->getItemInfo($item['item_template']);

      $typeId = $itemData['subclass'];
      $categoryId = $itemData['class'];
      if (2 === $categoryId) {
        $type = $itemService->getWeaponType($typeId);
      }
      else {
        $type = $itemService->getArmorType($typeId);
      }

      $items[$item['slot']] = [
        'name' => $itemData['name'],
        'color' => $itemService->getQualityColor($itemData['quality']),
        'type' => $type,
        'isWeapon' => $categoryId === 2,
        'stats' => $itemService->getItemStats($itemData),
        'id' => $itemData['entry'],
        'itemLevel' => $itemData['itemlevel'],
        'dmgMin1' => $itemData['dmgMin1'],
        'dmgMax1' => $itemData['dmgMax1'],
        'durability' => $itemData['maxdurability'],
        'requiredLevel' => $itemData['requiredlevel'],
        'delay' => $itemData['delay'] / 1000,
        'armor' => $itemData['armor'],
        'sellPrice' => $itemData['sellprice'],
        'description' => $itemData['description'],
      ];
    }

    $client = \Drupal::httpClient();
    $displayItems = $itemService->getDisplayItems($data, $itemService, $client);
    $guilds = $db
      ->query(
        'SELECT 
    *
FROM
    guild_member member
        LEFT JOIN
    guild guild ON member.guildid = guild.guildid  
WHERE
    member.guid = \'' . $data[0]['guid'] . '\';'
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    $raceId = $data[0]['race'];
    $playerByte = $data[0]['playerBytes'];
    $playerByte2 = $data[0]['playerBytes2'];

    return [
      '#theme' => 'armory_detail',
      '#items' => $items,
      '#name' => $name,
      '#level' => $level,
      '#isAlliance' => in_array($raceId, self::ALLIANCE_RACES),
      '#race' => $this->t('Race' . $raceId . $data[0]['gender'], [], ['context' => 'warcraft_armory']),
      '#raceId' => $raceId,
      '#gender' => $data[0]['gender'],
      '#class' => $this->t('Class' . $data[0]['class'] . $data[0]['gender'], [], ['context' => 'warcraft_armory']),
      '#classId' => $data[0]['class'],
      '#guild' => !empty($guilds) ? reset($guilds)['name'] : '',
      '#isOnline' => $data[0]['online'],
      '#arenaPoints' => $data[0]['arenaPoints'],
      '#displayItems' => $displayItems,
      '#skinInfo' => [
        'hair' => ($playerByte >> 16) % 256,
        'hairColor' => ($playerByte >> 24) % 256,
        'face' => ($playerByte >> 8) % 256,
        'facial' => $playerByte2 % 256,
        'skin' => $playerByte % 256,
        'gender' => $data[0]['gender'],
        'race' => $raceId,
      ],
    ];
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function indexTitle() {
    return $this->t('Armory', [], ['context' => 'warcraft_armory']);
  }

  /**
   * @param $name
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function detailTitle($name) {
    return $this->t('Armory - @name', ['@name' => $name], ['context' => 'warcraft_armory']);
  }

}
