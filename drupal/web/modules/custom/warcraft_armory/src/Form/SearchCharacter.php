<?php

namespace Drupal\warcraft_armory\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SearchCharacter.
 */
class SearchCharacter extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_character';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => NULL,
      '#placeholder' => $this->t('Character name', [], ['context' => 'warcraft_armory']),
      '#default_value' => \Drupal::request()->query->get('name', ''),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search', [], ['context' => 'warcraft_armory']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('warcraft_armory.armory', ['name' => $form_state->getValue('name')]);
  }

}
