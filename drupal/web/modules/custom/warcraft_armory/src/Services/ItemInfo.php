<?php

namespace Drupal\warcraft_armory\Services;

use Drupal\Core\Site\Settings;
use Drupal\warcraft_core\Elastic\Elastic;

/**
 * Class ItemInfo
 *
 * @package Drupal\warcraft_armory\Services
 */
class ItemInfo {

  const MAP_SLOT_DISPLAY_ID = [
    0 => 1, //HEAD
    1 => NULL, //NECK
    2 => 3, //Shoulders
    3 => 4, //Body
    4 => NULL, //Chest
    5 => 6, //Waist
    6 => 7, //Legs
    7 => 8, //Feet
    8 => 9, //Wrists
    9 => 10, //Hands
    10 => NULL, //Finger1
    11 => NULL, //Finger2
    12 => NULL, //Trinket1
    13 => NULL, //Trinket2
    14 => 16, //Back
    15 => 21, //Main Hand
    16 => 23, //Off Hand
    17 => 26, //Ranged
    18 => 19, //Tabard
  ];

  /**
   * @param int $id
   *
   * @return array|null
   */
  public function getItemInfo(int $id): ?array {
    $data = \Drupal::httpClient()->get(Settings::get('api_items') . $id)->getBody()->getContents();
    $data = json_decode($data, TRUE);

    return $data;
  }

  /**
   * @param $data
   * @param \Drupal\warcraft_armory\Services\ItemInfo $itemService
   * @param \GuzzleHttp\Client $client
   *
   * @return array
   */
  public function getDisplayItems($data, $itemService, $client) {
    return array_map(
      function ($item) use ($itemService, $client) {
        $itemData = $itemService->getItemInfo($item['item_template']);
        $xml = $client->get(Settings::get('wow_head_url') . '/item=' . $itemData['entry'] . '&xml')
          ->getBody()
          ->getContents();
        /** @var \Symfony\Component\Serializer\Serializer $serializer */
        $serializer = \Drupal::service('serializer');
        $xml = $serializer->decode($xml, 'xml');

        if ($xml) {
          $displayId = array_key_exists('icon', $xml['item']) ? $xml['item']['icon']['@displayId'] : 0;
        }
        else {
          $displayId = 0;
        }

        $slot = (int) $item['slot'];

        return [
          'displayId' => $displayId,
          'position' => self::MAP_SLOT_DISPLAY_ID[$slot],
        ];
      },
      $data
    );
  }

  /**
   * Get stats info about item
   *
   * @param $data
   *
   * @return array
   */
  public function getItemStats($data) {
    $stats = [];

    for ($i = 1; $i <= 10; $i++) {
      $typeId = $data['statType' . $i];
      $statLabel = $this->getStatLabel($typeId);

      if (NULL === $statLabel) {
        continue;
      }

      $typeValue = $data['statValue' . $i];

      $stats[] = [
        'type' => $statLabel,
        'value' => $typeValue,
      ];
    }

    return $stats;
  }

  /**
   * @param $colorId
   *
   * @return string
   */
  public function getQualityColor($colorId) {
    $colors = [
      0 => 'grey',
      1 => 'white',
      2 => 'green',
      3 => 'blue',
      4 => 'purple',
      5 => 'orange',
      6 => 'red',
    ];

    return array_key_exists($colorId, $colors) ? $colors[$colorId] : '';
  }

  /**
   * @param $typeId
   *
   * @return string
   */
  public function getWeaponType($typeId) {
    $types = [
      0 => t('Axe 1H', [], ['context' => 'warcraft_armory']),
      1 => t('Axe 2H', [], ['context' => 'warcraft_armory']),
      2 => t('Bow', [], ['context' => 'warcraft_armory']),
      3 => t('Gun', [], ['context' => 'warcraft_armory']),
      4 => t('Mace 1H', [], ['context' => 'warcraft_armory']),
      5 => t('Mace 2H', [], ['context' => 'warcraft_armory']),
      6 => t('Polearm', [], ['context' => 'warcraft_armory']),
      7 => t('Sword 1H', [], ['context' => 'warcraft_armory']),
      8 => t('Sword 2H', [], ['context' => 'warcraft_armory']),
      10 => t('Staff', [], ['context' => 'warcraft_armory']),
      13 => t('First weapon', [], ['context' => 'warcraft_armory']),
      14 => t('Miscallaneous', [], ['context' => 'warcraft_armory']),
      15 => t('Dagger', [], ['context' => 'warcraft_armory']),
      16 => t('Thrown', [], ['context' => 'warcraft_armory']),
      17 => t('Spear', [], ['context' => 'warcraft_armory']),
      18 => t('Crossbow', [], ['context' => 'warcraft_armory']),
      19 => t('Wand', [], ['context' => 'warcraft_armory']),
      20 => t('Fishing pole', [], ['context' => 'warcraft_armory']),
    ];

    return array_key_exists($typeId, $types) ? $types[$typeId] : t('Unknown', [], ['context' => 'warcraft_armory']);
  }

  /**
   * @param $id
   *
   * @return string
   */
  public function getArmorType($id) {
    $types = [
      0 => t('Miscallaneous', [], ['context' => 'warcraft_armory']),
      1 => t('Cloth', [], ['context' => 'warcraft_armory']),
      2 => t('Leather', [], ['context' => 'warcraft_armory']),
      3 => t('Mail', [], ['context' => 'warcraft_armory']),
      4 => t('Plate', [], ['context' => 'warcraft_armory']),
      6 => t('Shield', [], ['context' => 'warcraft_armory']),
      7 => t('Libram', [], ['context' => 'warcraft_armory']),
      8 => t('Idol', [], ['context' => 'warcraft_armory']),
      9 => t('Totem', [], ['context' => 'warcraft_armory']),
    ];

    return array_key_exists($id, $types) ? $types[$id] : t('Unknown', [], ['context' => 'warcraft_armory']);
  }

  /**
   * @param $id
   *
   * @return string|null
   */
  private function getStatLabel($id) {
    $types = [
      1 => t('Health', [], ['context' => 'warcraft_armory']),
      3 => t('Agility', [], ['context' => 'warcraft_armory']),
      4 => t('Strength', [], ['context' => 'warcraft_armory']),
      5 => t('Intellect', [], ['context' => 'warcraft_armory']),
      6 => t('Spirit', [], ['context' => 'warcraft_armory']),
      7 => t('Stamina', [], ['context' => 'warcraft_armory']),
    ];

    return array_key_exists($id, $types) ? $types[$id] : NULL;
  }

}
