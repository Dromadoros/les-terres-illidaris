<?php

namespace Drupal\warcraft_armory\Command;

use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drush\Commands\DrushCommands;

/**
 * Class CrawlItemImages
 *
 * @package Drupal\warcraft_armory\Command
 */
class CrawlItemImages extends DrushCommands {

  /**
   * Echos back hello with the argument provided.
   *
   *
   * @command armory:fetch-images
   * @aliases a:fi
   * @usage armory:fetch-images
   *   Download all images from item db
   */
  public function fetchImages() {
    $dom = new \DOMDocument('1.0');
    Database::setActiveConnection('mangos');
    $db = Database::getConnection();

    $data = $db
      ->query('SELECT * FROM item_template WHERE class=2 OR class=4')
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as $item) {
      $dom->loadHTMLFile(Settings::get('wow_head_url') . '/item=' . $item['entry']);
      $this->getAttributeValue($dom, $item['entry']);
    }
  }

  /**
   * @param $dom
   * @param $id
   *
   * @return mixed
   */
  private function getAttributeValue($dom, $id) {
    /** @var \DOMElement $item */
    foreach ($dom->getElementsByTagName('link') as $item) {
      foreach ($item->attributes as $attr) {
        if ($attr->nodeName !== 'rel' || $attr->nodeValue !== 'image_src') {
          continue;
        }

        return @copy(
          $item->getAttribute('href'),
          drupal_get_path('theme', 'warcraft') . '/assets/images/armory/item_logo/' . $id . '.jpg'
        );
      }
    }

    return FALSE;
  }

}
