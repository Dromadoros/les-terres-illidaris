<?php

namespace Drupal\warcraft_achievement\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Achievements' Block.
 *
 * @Block(
 *   id = "achievements_block",
 *   admin_label = @Translation("Achievements block"),
 *   category = @Translation("Mangos"),
 * )
 */
class AchievementsBlock extends BlockBase {

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $accountId = $characterManager->getAccountId();
    $currentCategory = \Drupal::request()->query->get('category') ?: 'all';
    /** @var \Drupal\node\NodeInterface $currentNode */
    $currentNode = \Drupal::routeMatch()->getParameter('node');
    $currentUrl = $currentNode->toUrl()->toString();
    $myAchievements = [];

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $categories = $db->query(
      'SELECT type, COUNT(guid) as count FROM tbccharacters.achievement_template group by type'
    )->fetchAll(\PDO::FETCH_ASSOC);

    $countTotal = array_sum(array_column($categories, 'count'));

    array_unshift($categories, ['type' => 'all']);

    $categories = array_map(function($category) use ($currentCategory, $currentUrl, $countTotal) {
      $type = $category['type'];

      return [
        'active' => $type === $currentCategory,
        'count' => $type === 'all' ? $countTotal : $category['count'],
        'url' => $currentUrl . '?category=' . $type,
        'title' => t('achievement'.$type, [], ['context' => 'warcraft_achievement']),
        'me' => FALSE,
      ];
    }, $categories);

    if ($accountId) {
      $myAchievements = $db->query(
        'SELECT * FROM tbccharacters.achievement_template as at LEFT JOIN tbccharacters.achievements as a ON a.achievement = at.guid where a.player = ' . $accountId
      )->fetchAll(\PDO::FETCH_ASSOC);

      array_push($categories, [
        'active' => 'me' === $currentCategory,
        'count' => count($myAchievements),
        'url' => $currentUrl . '?category=me',
        'title' => t('Mes Hauts-Faits', [], ['context' => 'warcraft_achievement']),
        'me' => TRUE,
      ]);
    }

    if ('me' === $currentCategory) {
      foreach ($myAchievements as $achievement) {
        $achievements[] = [
          'title' => $achievement['title'],
          'description' => $achievement['description'],
          'points' => $achievement['points'],
          'active' => TRUE,
        ];
      }
    } else {
      $queryType = $currentCategory !== 'all' ? 'WHERE att.type = ' . $currentCategory : '';

      $achievementsTemplate = $db->query(
        'SELECT guid,type,points,title,description,TitleMale_koKR as title_reward from achievement_template as att LEFT JOIN character_title_template as ctt ON att.reward_title = ctt.ID '. $queryType .' ORDER BY title ASC'
      )->fetchAll(\PDO::FETCH_ASSOC);

      $playerhfIds = $db->query(
        'SELECT achievement FROM achievements WHERE achievements.player = \'' . $accountId . '\''
      )->fetchAll(\PDO::FETCH_ASSOC);

      foreach ($achievementsTemplate as $achievement) {
        $achievements[] = [
          'title' => $achievement['title'],
          'description' => $achievement['description'],
          'points' => $achievement['points'],
          'active' => in_array($achievement['guid'], array_column($playerhfIds, 'achievement')),
          'reward' => $achievement['title_reward'] ?: NULL,
        ];
      }
    }

    return [
      '#theme' => 'achievements',
      '#categories' => $categories,
      '#achievements' => $achievements,
      '#ranks' => $this->getRanking($db),
    ];
  }

  /**
   * @param $db
   *
   * @return array
   */
  private function getRanking($db) {
    $totalPointsPlayers = $db->query(
      'SELECT player, SUM(points) as total FROM tbccharacters.achievement_template as at LEFT JOIN tbccharacters.achievements as a ON a.achievement = at.guid group by player order by total DESC limit 11;'
    )->fetchAll(\PDO::FETCH_ASSOC);

    $totalPointsPlayers = array_filter($totalPointsPlayers, function($player) {
      return $player['player'] !== NULL;
    });

    Database::setActiveConnection('realmd');
    $db = Database::getConnection();

    return array_map(function($player) use ($db) {
      $accounts = $db->query('SELECT * from account where id = ' . $player['player'])->fetchAll(\PDO::FETCH_ASSOC);

      return [
        'username' => reset($accounts)['username'],
        'points' => $player['total'],
      ];
    }, $totalPointsPlayers);
  }

}
