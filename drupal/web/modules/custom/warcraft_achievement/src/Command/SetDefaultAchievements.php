<?php

namespace Drupal\warcraft_achievement\Command;

use Drupal\Core\Database\Database;
use Drush\Commands\DrushCommands;

/**
 * Class SetDefaultAchievements
 *
 * @package Drupal\warcraft_achievement\Command
 */
class SetDefaultAchievements extends DrushCommands {

  const HF_ID_ONE_70 = 408;

  const HF_ID_TWO_70 = 409;

  const HF_ID_FIVE_70 = 410;

  const HF_ID_QUEST_200 = 405;

  const HF_ID_QUEST_300 = 406;

  const HF_ID_QUEST_500 = 407;

  /**
   * @command hf:fetch
   * @aliases hf:f
   * @usage hf:fetch
   *   Set all default hf to players
   */
  public function fetchAchievements() {
    Database::setActiveConnection('realmd');
    $db = Database::getConnection();

    $accounts = $db
      ->query('SELECT * FROM account')
      ->fetchAll(\PDO::FETCH_ASSOC);

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $achievements = $db
      ->query('SELECT * FROM achievement_template')
      ->fetchAll(\PDO::FETCH_ASSOC);

    $this->output->writeln('STARTING FETCH ACHIEVEMENTS');
    $this->output->writeln('______________________________________');

    foreach ($accounts as $account) {
      $characters = $db
        ->query('SELECT * FROM characters WHERE account = ' . $account['id'])
        ->fetchAll(\PDO::FETCH_ASSOC);

      foreach ($achievements as $achievement) {
        $this->saveTitle($db, $account['id'], $achievement);
        $this->saveMegaHf($db, $account['id'], $achievement);

        switch ($achievement['type']) {
          case 2:
            $this->checkQuestAchievement($db, $characters, $achievement);
            break;
          case 3:
            $this->checkSpecialAchievement($db, $characters, $achievement);
            break;
          case 4:
            $this->checkItemAchievement($db, $characters, $achievement);
            break;
        }
      }
    }
  }

  /**
   * @param $db
   * @param $account
   * @param $achievement
   */
  private function checkQuestAchievement($db, $characters, $achievement) {
    if (empty($characters)) {
      return;
    }

    $accountId = reset($characters)['account'];

    if ($this->hasAlreadyAchievement($db, $accountId, $achievement['guid'])) {
      return;
    }

    foreach ($characters as $character) {
      $quest = $db
        ->query(
          'SELECT * FROM character_queststatus WHERE guid = ' . $character['guid'] . ' and quest = ' . $achievement['quest_id'] . ' and status = 1'
        )
        ->fetchAll(\PDO::FETCH_ASSOC);

      if (!empty($quest)) {
        $this->saveAchievement($db, $accountId, $achievement['guid']);

        return;
      }
    }
  }

  /**
   * @param $db
   * @param $account
   * @param $achievement
   */
  private function checkSpecialAchievement($db, $characters, $achievement) {
    if (empty($characters)) {
      return;
    }

    $accountId = reset($characters)['account'];

    if ($this->hasAlreadyAchievement($db, $accountId, $achievement['guid'])) {
      return;
    }

    $charactersMaxLevel = array_filter(
      $characters,
      function ($character) {
        return (int) $character['level'] === 70;
      }
    );

    //One player 70
    if (self::HF_ID_ONE_70 === (int) $achievement['guid'] && 1 <= count(
        $charactersMaxLevel
      )) {
      $this->saveAchievement($db, $accountId, self::HF_ID_ONE_70);
      return;
    }

    //Two player 70
    if (self::HF_ID_TWO_70 === (int) $achievement['guid'] && 2 <= count(
        $charactersMaxLevel
      )) {
      $this->saveAchievement($db, $accountId, self::HF_ID_TWO_70);
      return;
    }

    //Five player 70
    if (self::HF_ID_FIVE_70 === (int) $achievement['guid'] && 5 <= count(
        $charactersMaxLevel
      )) {
      $this->saveAchievement($db, $accountId, self::HF_ID_FIVE_70);
      return;
    }

    // QUESTS PART
    foreach ($characters as $character) {
      $quests = $db->query(
        'SELECT COUNT(*) as count FROM character_queststatus WHERE guid = ' . $character['guid'] . ' and status = 1'
      )
        ->fetchAll(\PDO::FETCH_ASSOC);

      $countQuest = (int) reset($quests)['count'];

      //200 quests
      if (self::HF_ID_QUEST_200 === (int) $achievement['guid'] && 200 <= $countQuest) {
        $this->saveAchievement($db, $accountId, self::HF_ID_QUEST_200);
        return;
      }

      //200 quests
      if (self::HF_ID_QUEST_300 === (int) $achievement['guid'] && 300 <= $countQuest) {
        $this->saveAchievement($db, $accountId, self::HF_ID_QUEST_300);
        return;
      }

      //200 quests
      if (self::HF_ID_QUEST_500 === (int) $achievement['guid'] && 500 <= $countQuest) {
        $this->saveAchievement($db, $accountId, self::HF_ID_QUEST_500);
        return;
      }
    }
  }

  /**
   * @param $db
   * @param $account
   * @param $achievement
   */
  private function checkItemAchievement($db, $characters, $achievement) {
    if (empty($characters)) {
      return;
    }

    $accountId = reset($characters)['account'];

    if ($this->hasAlreadyAchievement($db, $accountId, $achievement['guid'])) {
      return;
    }

    foreach ($characters as $character) {
      $items = $db->query(
        'SELECT * FROM character_inventory WHERE guid = ' . $character['guid'] . ' and item_template = ' . $achievement['item_id']
      )
        ->fetchAll(\PDO::FETCH_ASSOC);

      if (!empty($items)) {
        $this->saveAchievement($db, $accountId, $achievement['guid']);
        return;
      }
    }
  }

  /**
   * @param $db
   * @param $accountId
   * @param $achievement
   */
  private function saveTitle($db, $accountId, $achievement) {
    if (!$this->hasAlreadyAchievement($db, $accountId, $achievement['guid'])) {
      return;
    }

    $titleId = $achievement['reward_title'];

    if (!$titleId) {
      return;
    }

    $titles = $db->query(
      'SELECT * FROM character_titles WHERE `character` = ' . $accountId . ' and title_id = ' . $titleId
    )
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($titles)) {
      return;
    }

    $db->insert('character_titles')->fields(
      [
        'character' => $accountId,
        'title_id' => $titleId,
      ]
    )->execute();

    $this->output->writeln(
      'Account : ' . $accountId . ' | Title : ' . $titleId
    );
  }

  /**
   * @param $db
   * @param $accountId
   * @param $achievement
   */
  private function saveMegaHf($db, $accountId, $achievement) {
    $megaHf = $achievement['mega_hf'];

    if (!$megaHf) {
      return;
    }

    if ($this->hasAlreadyAchievement($db, $accountId, $megaHf)) {
      return;
    }

    $depPossessed = $db->query(
      'SELECT COUNT(*) as total FROM achievement_template RIGHT JOIN achievements ON achievement_template.guid=achievements.achievement where mega_hf = '. $megaHf .' and player = '. $accountId
    )
      ->fetchAll(\PDO::FETCH_ASSOC);

    $depTotal = $db->query(
      'SELECT COUNT(*) as total FROM achievement_template where mega_hf = ' . $megaHf
    )
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (reset($depTotal)['total'] !== reset($depPossessed)['total']) {
      return;
    }

    $db->insert('achievements')->fields(
      [
        'player' => $accountId,
        'achievement' => $megaHf,
        'date' => time(),
      ]
    )->execute();

    $this->output->writeln(
      'Account : ' . $accountId . ' | Mega HF : ' . $megaHf
    );
  }

  /**
   * @param $db
   * @param $accountId
   * @param $achievementId
   */
  private function saveAchievement($db, $accountId, $achievementId) {
    $db->insert('achievements')->fields(
      [
        'player' => $accountId,
        'achievement' => $achievementId,
        'date' => time(),
      ]
    )->execute();

    $this->output->writeln(
      'Account : ' . $accountId . ' | HF : ' . $achievementId
    );
  }

  /**
   * @param $db
   * @param $accountId
   * @param $achievementId
   *
   * @return bool
   */
  private function hasAlreadyAchievement($db, $accountId, $achievementId) {
    $achievements = $db
      ->query(
        'SELECT * FROM achievements WHERE player = ' . $accountId . ' AND achievement = ' . $achievementId
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    return !empty($achievements);
  }

}
