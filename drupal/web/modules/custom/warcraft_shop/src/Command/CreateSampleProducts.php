<?php

namespace Drupal\warcraft_shop\Command;

use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

/**
 * Class CreateSampleProducts
 *
 * @package Drupal\warcraft_shop\Command
 */
class CreateSampleProducts extends DrushCommands {

  const PRODUCTS_LABEL = [
    [
      'id' => '3306',
      'name' => 'Les Lames d\'Azzinoth',
      'description' => 'Achetez ces lames toute neuves venu droit du sud',
    ],
    [
      'id' => '35036',
      'name' => 'Le grand Sac du Temps',
      'description' => 'Vous en avez marre de devoir toujours vider vos sacs ? Celui-ci est fait pour vous alors !',
    ],
    [
      'id' => '1259',
      'name' => 'Barre de fer',
      'description' => 'Vous voulez acheter un item pourri ? Achetez donc cela.',
    ],
    [
      'id' => '14444',
      'name' => 'Les dagues de la Mort qui Tue',
      'description' => 'Comme dis le titre, la dague te OS',
    ],
    [
      'id' => '22223',
      'name' => 'La bague de mariage',
      'description' => 'Offrez une bague incroyable à votre amoureuse',
    ],
    [
      'id' => '14342',
      'name' => 'Le costume de l\'année',
      'description' => 'Ayez le swag incroyable',
    ],
  ];

  /**
   * Set default votes points to user by their monnaie
   *
   *
   * @command warcraft:generate:products
   * @aliases w:g:p
   * @usage warcraft:generate:products
   *   Generate products sample
   */
  public function generate() {
    foreach (self::PRODUCTS_LABEL as $product) {
      $node = Node::create([
        'status' => TRUE,
        'type' => 'product',
        'title' => $product['name'],
        'field_product_item_id' => $product['id'],
        'field_product_price' => rand(30, 300),
        'field_product_description' => $product['description'],
        'field_product_item_name' => $product['name'],
        'field_product_type' => 'item',
      ]);

      $node->save();
    }
  }

}
