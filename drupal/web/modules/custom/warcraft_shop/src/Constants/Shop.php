<?php

namespace Drupal\warcraft_shop\Constants;

/**
 * Class Shop
 *
 * @package Drupal\warcraft_shop\Constants
 */
final class Shop {

  //HORDE
  const REPUTATIONS_HORDE = 67;

  const REPUTATIONS_UNDERCITY = 68;

  const REPUTATIONS_ORGRIMMAR = 76;

  const REPUTATIONS_THUNDERBLUFF = 81;

  const REPUTATIONS_TROLLS = 530;

  const REPUTATIONS_SILVERMOON = 911;

  //ALLIANCE
  const REPUTATIONS_ALLIANCE = 469;

  const REPUTATIONS_STORMWIND = 72;

  const REPUTATIONS_DARNASSUS = 69;

  const REPUTATIONS_EXODAR = 930;

  const REPUTATIONS_IRONFORGE = 47;

  const REPUTATIONS_GNOMEREGAN = 54;

  const RECUP_ITEMSET = [
    'war' => [
      'label' => 'Guerrier',
    ],
    'war_arme' => [
      'set' => '25977:1 25914:1 27725:1 25927:1 30959:1 30352:1 30267:1 29807:1 29980:1 29786:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31785:1 25971:1',
      'label' => 'Guerrier Arme',
    ],
    'war_fury' => [
      'set' => '25977:1 25914:1 27725:1 25927:1 30959:1 30352:1 30267:1 29807:1 29980:1 29786:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '29108:1 29108:1 25971:1',
      'label' => 'Guerrier Fury',
    ],
    'war_tank' => [
      'set' => '29946:1 25914:1 25522:1 27730:1 30270:1 30225:1 30264:1 30380:1 27718:1 32866:1',
      'trinkets' => '30006:1 30006:1 25996:1 25787:1',
      'weapon' => '30278:1 25624:1 25971:1',
      'label' => 'Guerrier Protection',
    ],
    'paladin' => [
      'label' => 'Paladin',
    ],
    'paladin_vindi' => [
      'set' => '25977:1 25914:1 27725:1 25927:1 30959:1 30352:1 30267:1 29807:1 29980:1 29786:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31785:1 28065:1',
      'label' => 'Paladin Vindicte',
    ],
    'paladin_tank' => [
      'set' => '29946:1 25914:1 25522:1 27730:1 30270:1 30225:1 30264:1 30380:1 27718:1 32866:1',
      'trinkets' => '30006:1 30006:1 25996:1 25787:1',
      'weapon' => '30278:1 25624:1 28065:1',
      'label' => 'Paladin Protect',
    ],
    'paladin_heal' => [
      'set' => '25530:1 30276:1 30381:1 30338:1 30296:1 30400:1 25566:1 30330:1 29774:1 30968:1',
      'trinkets' => '29793:1 29793:1 30293:1 25634:1',
      'weapon' => '31475:1 29917:1 28065:1',
      'label' => 'Paladin Heal',
    ],
    'shaman' => [
      'label' => 'Chaman',
    ],
    'shaman_elem' => [
      'set' => '29773:1 29775:1 31314:1 30971:1 30363:1 25592:1 30224:1 30342:1 30957:1 30958:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 29917:1 28066:1 5175:1 5176:1 5177:1 5178:1',
      'label' => 'Chaman Elem',
    ],
    'shaman_ambi' => [
      'set' => '30955:1 30981:1 30952:1 29792:1 30947:1 30956:1 30336:1 30950:1 32870:1 30953:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '29787:1 29787:1 28066:1 5175:1 5176:1 5177:1 5178:1',
      'label' => 'Chaman Amélio',
    ],
    'shaman_heal' => [
      'set' => '31713:1 30276:1 31314:1 30338:1 25556:1 25592:1 31515:1 30342:1 30957:1 30958:1',
      'trinkets' => '29793:1 29793:1 30293:1 25634:1',
      'weapon' => '31475:1 29917:1 28066:1 5175:1 5176:1 5177:1 5178:1',
      'label' => 'Chaman Heal',
    ],
    'hunt' => [
      'label' => 'Chasseur',
    ],
    'hunt_beast' => [
      'set' => '30955:1 30981:1 30952:1 29792:1 30947:1 30956:1 30336:1 30950:1 32870:1 30953:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31785:1 30226:1',
      'label' => 'Chasseur Bête',
    ],
    'hunt_survival' => [
      'set' => '30955:1 30981:1 30952:1 29792:1 30947:1 30956:1 30336:1 30950:1 32870:1 30953:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31785:1 30226:1',
      'label' => 'Chasseur Survie',
    ],
    'hunt_preci' => [
      'set' => '30955:1 30981:1 30952:1 29792:1 30947:1 30956:1 30336:1 30950:1 32870:1 30953:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31785:1 30226:1',
      'label' => 'Chasseur Précision',
    ],
    'druid' => [
      'label' => 'Druide',
    ],
    'druid_feral' => [
      'set' => '30362:1 30981:1 29810:1 29792:1 32869:1 30332:1 32865:1 29772:1 30272:1 30401:1',
      'trinkets' => '30339:1 30339:1 29776:1 31617:1',
      'weapon' => '31414:1 28064:1',
      'label' => 'Druide Feral',
    ],
    'druid_poule' => [
      'set' => '30946:1 29775:1 30262:1 30971:1 30945:1 29955:1 29784:1 31513:1 30290:1 31112:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 28064:1',
      'label' => 'Druide Equilibre',
    ],
    'druid_heal' => [
      'set' => '30946:1 30276:1 31538:1 30338:1 30945:1 29955:1 31313:1 25583:1 30290:1 30335:1',
      'trinkets' => '29793:1 29793:1 30293:1 25634:1',
      'weapon' => '31475:1 31732:1 28064:1',
      'label' => 'Druide Heal',
    ],
    'druid_tank' => [
      'set' => '29979:1 30981:1 29999:1 29792:1 32869:1 30332:1 32865:1 30285:1 30272:1 30266:1',
      'trinkets' => '30339:1 30339:1 25996:1 25787:1',
      'weapon' => '31414:1 28064:1',
      'label' => 'Druide Tank',
    ],
    'rogue' => [
      'label' => 'Voleur',
    ],
    'rogue_finesse' => [
      'set' => '29979:1 31790:1 31148:1 31255:1 28172:1 31175:1 24365:1 31131:1 25821:1 25946:1',
      'trinkets' => ' 27925:1 30973:1 25633:1 25628:1',
      'weapon' => '32781:1 31703:1 29212:1',
      'label' => 'Voleur Finesse',
    ],
    'rogue_combat' => [
      'set' => '29979:1 31790:1 31148:1 31255:1 28172:1 31175:1 24365:1 31131:1 25821:1 25946:1',
      'trinkets' => ' 27925:1 30973:1 25633:1 25628:1',
      'weapon' => '32781:1 31703:1 29212:1',
      'label' => 'Voleur Combat',
    ],
    'rogue_assa' => [
      'set' => '29979:1 31790:1 31148:1 31255:1 28172:1 31175:1 24365:1 31131:1 25821:1 25946:1',
      'trinkets' => ' 27925:1 30973:1 25633:1 25628:1',
      'weapon' => '32781:1 31703:1 29212:1',
      'label' => 'Voleur Assa',
    ],
    'mage' => [
      'label' => 'Mage',
    ],
    'mage_givre' => [
      'set' => '30922:1 29775:1 29954:1 30971:1 30518:1 30927:1 30930:1 30923:1 30517:1 30519:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 32872:1',
      'label' => 'Mage Givre',
    ],
    'mage_fire' => [
      'set' => '30922:1 29775:1 29954:1 30971:1 30518:1 30927:1 30930:1 30923:1 30517:1 30519:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 32872:1',
      'label' => 'Mage Feu',
    ],
    'mage_arcane' => [
      'set' => '30922:1 29775:1 29954:1 30971:1 30518:1 30927:1 30930:1 30923:1 30517:1 30519:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 32872:1',
      'label' => 'Mage Arcane',
    ],
    'warlock' => [
      'label' => 'Démoniste',
    ],
    'warlock_affli' => [
      'set' => '30515:1 31693:1 30514:1 25489:1 28052:1 31481:1 24450:1 30516:1 31783:1 31312:1',
      'trinkets' => ' 24079:1 25505:1 27922:1 25620:1',
      'weapon' => '25760:1 27540:1 22243:1 22244:1',
      'label' => 'Démoniste Affli',
    ],
    'warlock_demono' => [
      'set' => '30515:1 31693:1 30514:1 25489:1 28052:1 31481:1 24450:1 30516:1 31783:1 31312:1',
      'trinkets' => ' 24079:1 25505:1 27922:1 25620:1',
      'weapon' => '25760:1 27540:1 22243:1 22244:1',
      'label' => 'Démoniste Démonologie',
    ],
    'warlock_destru' => [
      'set' => '30515:1 31693:1 30514:1 25489:1 28052:1 31481:1 24450:1 30516:1 31783:1 31312:1',
      'trinkets' => ' 24079:1 25505:1 27922:1 25620:1',
      'weapon' => '25760:1 27540:1 22243:1 22244:1',
      'label' => 'Démoniste Destru',
    ],
    'priest' => [
      'label' => 'Prêtre',
    ],
    'priest_shadow' => [
      'set' => '30922:1 29775:1 29954:1 30971:1 30518:1 30927:1 30930:1 30923:1 30517:1 30519:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 32872:1',
      'label' => 'Prêtre Ombre',
    ],
    'priest_disci' => [
      'set' => '30922:1 29775:1 29954:1 30971:1 30518:1 30927:1 30930:1 30923:1 30517:1 30519:1',
      'trinkets' => '29793:1 29793:1 30340:1 31615:1',
      'weapon' => '25543:1 31699:1 32872:1',
      'label' => 'Prêtre Disci',
    ],
    'priest_sacry' => [
      'set' => '30922:1 30276:1 29954:1 30338:1 31433:1 30927:1 31111:1 30923:1 25574:1 30284:1',
      'trinkets' => '29793:1 29793:1 30293:1 25634:1',
      'weapon' => '31475:1 31732:1 29779:1',
      'label' => 'Prêtre sacré',
    ],
  ];

  //ITEMSET
  const ITEMSET_MAGE = '16685:1 16683:1 16686:1 16684:1 16687:1 16689:1 16688:1 16682:1';

  const ITEMSET_WARLOCK = '16702:1 16703:1 16699:1 16701:1 16700:1 16704:1 16698:1 16705:1 22243:1 22244:1';

  const ITEMSET_PRIEST = '16696:1 16691:1 16697:1 16693:1 16692:1 16695:1 16694:1 16690:1';

  const ITEMSET_ROGUE = '16713:1 16711:1 16710:1 16721:1 16708:1 16709:1 16712:1 16707:1';

  const ITEMSET_DRUID = '16716:1 16715:1 16714:1 16720:1 16706:1 16718:1 16719:1 16717:1';

  const ITEMSET_SHAMAN = '16673:1 16670:1 16671:1 16667:1 16672:1 16668:1 16669:1 16666:1 5175:1 5176:1 5177:1 5178:1';

  const ITEMSET_HUNT = '16680:1 16675:1 16681:1 16677:1 16674:1 16678:1 16679:1 16676:1';

  const ITEMSET_WAR = '16736:1 16734:1 16735:1 16730:1 16737:1 16731:1 16732:1 16733:1';

  const ITEMSET_PALADIN = '16723:1 16725:1 16722:1 16726:1 16724:1 16728:1 16729:1 16727:1';

  //WEAPON
  const WEAPON_SHIELD_MANA = '13083:1';

  const WEAPON_SWORD_2H = '25987:1';

  const WEAPON_MACE_1H_MANA = '18048:1';

  const WEAPON_DAGGERS = '5267:1 19542:1';

  const WEAPON_BOW = '12653:1';

  const WEAPON_GUN = '16007:1';

  const WEAPON_STAFF = '18534:1';

  const WEAPON_WAND = '16997:1';

  const WEAPON_RELIQUE = '22397:1';

  const WEAPON_TOTEM = '22395:1';

  const WEAPON_LIBRAM = '22401:1';

  //SKILL
  const SKILL_DEFENSE = '95';

  const SKILL_SHIELD = '433';

  const SKILL_SWORD_1H = '43';

  const SKILL_SWORD_2H = '55';

  const SKILL_MACE_1H = '54';

  const SKILL_DAGGERS = '173';

  const SKILL_AXE_1H = '44';

  const SKILL_AXE_2H = '172';

  const SKILL_BOW = '45';

  const SKILL_GUN = '46';

  const SKILL_STAFF = '136';

  const SKILL_WAND = '228';

  const SKILL_POISONS = '40';

  const SKILL_LOCKPICKING = '633';

  //PROFESSIONS
  const PRO_ENCHANT = [
    'skill' => '333',
    'spell' => '13920',
  ];

  const PRO_ALCHIMIE = [
    'skill' => '171',
    'spell' => '11611',
  ];

  const PRO_FORGE = [
    'skill' => '164',
    'spell' => '9785',
  ];

  const PRO_INGENIEUR = [
    'skill' => '202',
    'spell' => '12656',
  ];

  const PRO_HERBORISTE = [
    'skill' => '182',
    'spell' => '11993',
  ];

  const PRO_TDC = [
    'skill' => '165',
    'spell' => '10662',
  ];

  const PRO_JOA = [
    'skill' => '755',
    'spell' => '28895',
  ];

  const PRO_MINEUR = [
    'skill' => '186',
    'spell' => '10248',
  ];

  const PRO_DEPECEUR = [
    'skill' => '393',
    'spell' => '10768',
  ];

  const PRO_TAILLEUR = [
    'skill' => '197',
    'spell' => '12180',
  ];

  const PRO_PECHE = [
    'skill' => '356',
    'spell' => '18248',
  ];

  //BAGS
  const BAG_12 = '1725:3';

  const MOUNT_HORDE = '18796:1';
  const MOUNT_ALLIANCE = '18777:1';

  const MOUNT_SLOW_HORDE = '5665:1';
  const MOUNT_SLOW_ALLIANCE = '2414:1';
}
