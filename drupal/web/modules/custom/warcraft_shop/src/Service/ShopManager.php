<?php

namespace Drupal\warcraft_shop\Service;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Update;
use Drupal\warcraft_core\Constants\Classes;
use Drupal\warcraft_core\Constants\Races;
use Drupal\warcraft_shop\Constants\Shop;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ShopManager
 *
 * @package Drupal\warcraft_shop\Service
 */
class ShopManager {

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $character
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param $cost
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function sesame($db, $character, $user, $cost, $node) {
    $userPoints = $user->get('field_user_points')->value;
    if ($character['level'] >= 58) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Your character is higher or same than 58',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if (!(bool) $character['totaltime']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to log-in once in game',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }
    if ((bool) $character['online']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to disconnect your character first.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    $mainCharactersData = [
      'level' => 58,
      'xp' => 0,
      'money' => 4000000,
    ];
    $query = $db->update('character_reputation');

    $query
      ->condition($this->getFactionsConditionGroup($character, $query))
      ->condition('guid', $character['guid'])
      ->fields(
        [
          'standing' => 20000,
          'flags' => 17,
        ]
      )
      ->execute();

    $db->update('characters')->condition('guid', $character['guid'])->fields(
      array_merge($mainCharactersData, $this->getSesamePosition($character))
    )->execute();

    //MOUNT 60%
    $this->learnSpell($db, $character['guid'], 33388);
    $this->updateSkill($db, $character['guid'], 762, 75);

    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();

    $characterName = $character['name'];

    $this->updateSesameSkills($db, $character);

    $command = ".send items " . $characterName . " \"" . t(
        '[BOUTIQUE] Itemset sesame',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $this->getClassItemset($character);

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[BOUTIQUE] Weapon sesame',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $this->getClassWeapon(
        $character
      );

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[BOUTIQUE] Bags sesame',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . Shop::BAG_12;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $isAlliance = in_array($character['race'], Races::ALLIANCE_RACES_IDS);
    $mount = $isAlliance ? Shop::MOUNT_SLOW_ALLIANCE : Shop::MOUNT_SLOW_HORDE;

    $command = ".send items " . $characterName . " \"" . t(
        '[BOUTIQUE] Monture',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $mount;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $this->sendMailShop($node->getTitle(), $cost, $user->getUsername());
    $this->learnClassSpells($db, $character);

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'You can connect now in game and enjoye your sesame',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $character
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param $cost
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function switchName($db, $character, $user, $cost, $node) {
    $userPoints = $user->get('field_user_points')->value;
    if ((bool) $character['online']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to disconnect your character first.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    $db->update('characters')->condition('guid', $character['guid'])->fields(
      [
        'at_login' => 1,
      ]
    )->execute();

    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();

    $this->sendMailShop($node->getTitle(), $cost, $user->getUsername());

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'You can connect now in game and rename your character',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $character
   * @param $currentSpe
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param $cost
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function boost($db, $character, $currentSpe, $user, $cost, $node) {
    $userPoints = $user->get('field_user_points')->value;
    if ((bool) $character['online']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to disconnect your character first.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if (!$this->doBoost($character['guid'], [], $currentSpe)) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Something went wrong ... Contact an administrator',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();

    $this->sendMailShop($node->getTitle(), $cost, $user->getUsername());

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'You can connect now in game with your new boost 70!',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param $characterId
   * @param $professions
   * @param $spe
   *
   * @return bool
   * @throws \Exception
   */
  public function doBoost($characterId, $professions, $spe) {
    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $character = $characterManager->getCharacterData($characterId);

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    if (!(bool) $character['totaltime']) {
      \Drupal::messenger()->addError(
        t(
          'You need to log-in once in game',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }
    if ((bool) $character['online']) {
      \Drupal::messenger()->addError(
        t(
          'Le personnage doit être deconnecté',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }
    if ((int) $character['level'] === 70) {
      \Drupal::messenger()->addError(
        t(
          'Votre personnage est déjà niveau 70',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }

    $isAlliance = in_array($character['race'], Races::ALLIANCE_RACES_IDS);

    $mainCharactersData = [
      'level' => 70,
      'xp' => 0,
      'money' => 20000000,
      'taximask' => $isAlliance ?
        '3456411898 2148078929 805356359 2605711384 137366529 262240 1052676 0' :
        '830150144 315656864 449720 3869245476 3227522050 262180 1048576 0',
    ];
    $query = $db->update('character_reputation');

    $query
      ->condition($this->getFactionsConditionGroup($character, $query))
      ->condition('guid', $character['guid'])
      ->fields(
        [
          'standing' => 20000,
          'flags' => 17,
        ]
      )
      ->execute();

    $db->update('characters')->condition('guid', $character['guid'])->fields(
      array_merge(
        $mainCharactersData,
        $this->getSesamePosition($character)
      )
    )->execute();

    if (!empty($professions)) {
      foreach ($professions as $profession) {
        $this->buyProfession($db, $characterId, $profession['value']);
      }
    }

    $this->learnSpell($db, $characterId, 34091);

    $characterName = $character['name'];

    $this->updateSesameSkills($db, $character, 350);
    $itemset = Shop::RECUP_ITEMSET[$spe];

    if (!$itemset) {
      \Drupal::messenger()->addError(
        t(
          'Problème lors du boost 70',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Itemset',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['set'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Bagues et bijoux',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['trinkets'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Armes',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['weapon'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Sacs',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . Shop::BAG_12;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $mount = $isAlliance ? Shop::MOUNT_ALLIANCE : Shop::MOUNT_HORDE;

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Monture',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $mount;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $this->learnClassSpells($db, $character);

    return TRUE;
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $character
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param $cost
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function mount($db, $character, $user, $cost, $node) {
    $userPoints = $user->get('field_user_points')->value;
    $characterId = $character['guid'];

    if ((bool) $character['online']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to disconnect your character first.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if (!(bool) $character['totaltime']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to log-in once in game',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if ($character['level'] != 70) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Votre personnage doit être niveau 70.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if ($this->hasSkillAndValue($db, $character, 762, 300)) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Vous possédez déjà la compétence "Monte" à son maximum',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    //Learn Artisan riding
    $this->learnSpell($db, $characterId, 34091);
    $this->updateSkill($db, $characterId, 762, 300);

    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();

    $this->sendMailShop($node->getTitle(), $cost, $user->getUsername());

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'Votre compétence monte a été up à son maximum, bon jeu !',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $character
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param $cost
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function fly($db, $character, $user, $cost, $node) {
    $userPoints = $user->get('field_user_points')->value;
    $characterId = $character['guid'];

    if ((bool) $character['online']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to disconnect your character first.',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    if (!(bool) $character['totaltime']) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You need to log-in once in game',
            [],
            ['context' => 'warcraft_shop']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_shop']),
        ]
      );
    }

    $isAlliance = in_array($character['race'], Races::ALLIANCE_RACES_IDS);
    $taxiMask = $isAlliance ?
      '3456411898 2148078929 805356359 2605711384 137366529 262240 1052676 0' :
      '830150144 315656864 449720 3869245476 3227522050 262180 1048576 0';

    $db->update('characters')->condition('guid', $character['guid'])->fields(
      [
        'taximask' => $taxiMask,
      ]
    )->execute();

    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();

    $this->sendMailShop($node->getTitle(), $cost, $user->getUsername());

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'Votre personnage a désormais accès à tous les fly.',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param $character
   * @param \Drupal\Core\Database\Query\Update $query
   *
   * @return \Drupal\Core\Database\Query\Condition|\Drupal\Core\Database\Query\ConditionInterface
   */
  public function getFactionsConditionGroup($character, Update $query) {
    $isAlliance = in_array($character['race'], Races::ALLIANCE_RACES_IDS);

    $hordeFactionsGroup = $query
      ->orConditionGroup()
      ->condition('faction', Shop::REPUTATIONS_HORDE)
      ->condition('faction', Shop::REPUTATIONS_UNDERCITY)
      ->condition('faction', Shop::REPUTATIONS_ORGRIMMAR)
      ->condition('faction', Shop::REPUTATIONS_THUNDERBLUFF)
      ->condition('faction', Shop::REPUTATIONS_TROLLS)
      ->condition('faction', Shop::REPUTATIONS_SILVERMOON);

    $allianceFactionsGroup = $query
      ->orConditionGroup()
      ->condition('faction', Shop::REPUTATIONS_ALLIANCE)
      ->condition('faction', Shop::REPUTATIONS_STORMWIND)
      ->condition('faction', Shop::REPUTATIONS_DARNASSUS)
      ->condition('faction', Shop::REPUTATIONS_EXODAR)
      ->condition('faction', Shop::REPUTATIONS_GNOMEREGAN)
      ->condition('faction', Shop::REPUTATIONS_IRONFORGE);

    return $isAlliance ? $allianceFactionsGroup : $hordeFactionsGroup;
  }

  /**
   * @param $character
   *
   * @return array
   */
  public function getSesamePosition($character) {
    return [
      'position_x' => '-12999.927734',
      'position_y' => '-1286.234009',
      'position_z' => '43.822937',
      'map' => '0',
      'orientation' => '5.720457',
      'zone' => '33',
    ];
  }

  /**
   * @param $character
   *
   * @return string
   */
  public function getClassItemset($character) {
    $class = $character['class'];
    $item = '';

    switch ($class) {
      case Classes::WAR:
        $item = Shop::ITEMSET_WAR;
        break;
      case Classes::PALADIN:
        $item = Shop::ITEMSET_PALADIN;
        break;
      case Classes::HUNTER:
        $item = Shop::ITEMSET_HUNT;
        break;
      case Classes::SHAMAN:
        $item = Shop::ITEMSET_SHAMAN;
        break;
      case Classes::ROGUE:
        $item = Shop::ITEMSET_ROGUE;
        break;
      case Classes::MAGE:
        $item = Shop::ITEMSET_MAGE;
        break;
      case Classes::WARLOCK:
        $item = Shop::ITEMSET_WARLOCK;
        break;
      case Classes::PRIEST:
        $item = Shop::ITEMSET_PRIEST;
        break;
      case Classes::DRUID:
        $item = Shop::ITEMSET_DRUID;
        break;
    }

    return $item;
  }

  /**
   * @param $character
   *
   * @return string
   */
  public function getClassWeapon($character) {
    $class = $character['class'];
    $weaponHunt = (int) $character['race'] === 3 ? Shop::WEAPON_GUN : Shop::WEAPON_BOW;
    $item = '';

    switch ($class) {

      case Classes::WAR:
        $item = Shop::WEAPON_SWORD_2H;
        break;
      case Classes::PALADIN:
        $item = Shop::WEAPON_SWORD_2H . ' ' . Shop::WEAPON_LIBRAM;
        break;
      case Classes::SHAMAN:
        $item = Shop::WEAPON_MACE_1H_MANA . ' ' . Shop::WEAPON_SHIELD_MANA . ' ' . Shop::WEAPON_TOTEM;
        break;
      case Classes::HUNTER:
        $item = Shop::WEAPON_SWORD_2H . ' ' . $weaponHunt;
        break;
      case Classes::ROGUE:
        $item = Shop::WEAPON_DAGGERS;
        break;
      case Classes::DRUID:
        $item = Shop::WEAPON_STAFF . ' ' . Shop::WEAPON_RELIQUE;
        break;
      case Classes::PRIEST:
      case Classes::MAGE:
      case Classes::WARLOCK:
        $item = Shop::WEAPON_STAFF . ' ' . Shop::WEAPON_WAND;
        break;
    }

    return $item;
  }

  /**
   * @param $character
   *
   * @return array
   */
  private function getClassSkill($character) {
    $class = $character['class'];
    $skillHunt = (int) $character['race'] === 3 ? Shop::SKILL_GUN : Shop::SKILL_BOW;
    $skill = '';

    switch ($class) {
      case Classes::WAR:
      case Classes::PALADIN:
        $skill = [
          Shop::SKILL_SWORD_1H,
          Shop::SKILL_SWORD_2H,
          Shop::SKILL_AXE_1H,
          Shop::SKILL_AXE_2H,
        ];
        break;
      case Classes::SHAMAN:
        $skill = [
          Shop::SKILL_MACE_1H,
          Shop::SKILL_DAGGERS,
          Shop::SKILL_SHIELD,
        ];
        break;
      case Classes::HUNTER:
        $skill = [
          Shop::SKILL_SWORD_2H,
          Shop::SKILL_AXE_1H,
          Shop::SKILL_SWORD_1H,
          Shop::SKILL_AXE_2H,
          $skillHunt,
        ];
        break;
      case Classes::ROGUE:
        $skill = [
          Shop::SKILL_DAGGERS,
          Shop::SKILL_SWORD_1H,
          Shop::SKILL_POISONS,
          Shop::SKILL_LOCKPICKING,
        ];
        break;
      case Classes::DRUID:
        $skill = [Shop::SKILL_STAFF, Shop::SKILL_DAGGERS];
        break;
      case Classes::PRIEST:
      case Classes::MAGE:
      case Classes::WARLOCK:
        $skill = [Shop::SKILL_STAFF, Shop::SKILL_WAND, Shop::SKILL_DAGGERS];
        break;
    }

    $skill[] = Shop::SKILL_DEFENSE;

    return $skill;
  }

  /**
   * @param $character
   *
   * @return array
   */
  public function learnClassSpells($db, $character) {
    $class = $character['class'];
    $spells = [];
    $characterId = $character['guid'];

    switch ($class) {
      case Classes::WAR:
        $spells = [71, 2458, 23920, 20252, 7386, 355];break;
      case Classes::PALADIN:
        $spells = [7328];
        break;
      case Classes::SHAMAN:
        $spells = [8071, 3599, 5394];
        break;
      case Classes::HUNTER:
        $spells = [1515, 6991, 982, 883, 2641, 136, 2649, 5149, 14916];
        break;
      case Classes::ROGUE:
        $spells = [2842, 1804];
        break;
      case Classes::DRUID:
        $spells = [6795, 6807, 5487, 1066, 18960];
        break;
      case Classes::PRIEST:
      case Classes::MAGE:
        break;
      case Classes::WARLOCK:
        $spells = [697, 712, 691, ];
        break;
    }

    foreach ($spells as $spell) {
      $this->learnSpell($db, $characterId, $spell);
    }

    return $spells;
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $characterId
   * @param $professionName
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function buyProfession($db, $characterId, $professionName) {
    $profession = $this->getProfessionData($professionName);

    if (!$profession) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Problème avec la profession donnée.',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    $professionsData = [
      Shop::PRO_ENCHANT['skill'],
      Shop::PRO_ALCHIMIE['skill'],
      Shop::PRO_FORGE['skill'],
      Shop::PRO_INGENIEUR['skill'],
      Shop::PRO_HERBORISTE['skill'],
      Shop::PRO_TDC['skill'],
      Shop::PRO_JOA['skill'],
      Shop::PRO_MINEUR['skill'],
      Shop::PRO_DEPECEUR['skill'],
      Shop::PRO_TAILLEUR['skill'],
    ];

    $characterProfessions = $db->query(
      'SELECT * FROM character_skills WHERE character_skills.guid = ' . $characterId . ' AND character_skills.skill IN (' . implode(
        ',',
        $professionsData
      ) . ')'
    )->fetchAll(\PDO::FETCH_ASSOC);

    if (count($characterProfessions) >= 2) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Vous possédez déjà 2 métiers.',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    $resultSkill = $db->select('character_skills', 'cs')
      ->fields('cs', ['guid', 'value'])
      ->condition('guid', $characterId)
      ->condition('skill', $profession['skill'])
      ->execute()
      ->fetchAssoc();

    $resultSpell = $db->select('character_spell', 'csp')
      ->fields('csp', ['guid'])
      ->condition('guid', $characterId)
      ->condition('spell', $profession['spell'])
      ->execute()
      ->fetchAssoc();

    if ($resultSkill && (int) $resultSkill['value'] >= 300) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Vous avez déjà un métier 300+.',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    try {
      $this->updateProfession(
        $db,
        $characterId,
        $professionName,
        $resultSpell,
        $resultSkill
      );
    } catch (\Exception $e) {
      \Drupal::logger('warcraft_shop')->error($e);

      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Une erreur s\'est produite',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_shop']),
        'message' => t(
          'Félicitations, votre métier a bien été mis à jour.',
          [],
          ['context' => 'warcraft_shop']
        ),
      ]
    );
  }

  /**
   * @param $professionName
   *
   * @return array
   */
  private function getProfessionData($professionName) {
    switch ($professionName) {
      case 'alchimie':
        $profession = Shop::PRO_ALCHIMIE;
        break;
      case 'forge':
        $profession = Shop::PRO_FORGE;
        break;
      case 'enchanteur':
        $profession = Shop::PRO_ENCHANT;
        break;
      case 'ingenieur':
        $profession = Shop::PRO_INGENIEUR;
        break;
      case 'herboriste':
        $profession = Shop::PRO_HERBORISTE;
        break;
      case 'tdc':
        $profession = Shop::PRO_TDC;
        break;
      case 'joa':
        $profession = Shop::PRO_JOA;
        break;
      case 'mineur':
        $profession = Shop::PRO_MINEUR;
        break;
      case 'depeceur':
        $profession = Shop::PRO_DEPECEUR;
        break;
      case 'tailleur':
        $profession = Shop::PRO_TAILLEUR;
        break;
      case 'peche':
        $profession = Shop::PRO_PECHE;
        break;
      default:
        $profession = NULL;
        break;
    }

    return $profession;
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $characterId
   * @param $professionName
   * @param $hasSpell
   * @param $hasSkill
   *
   * @throws \Exception
   */
  public function updateProfession(
    $db,
    $characterId,
    $professionName,
    $hasSpell = FALSE,
    $hasSkill = FALSE
  ) {
    $profession = $this->getProfessionData($professionName);

    if (!$profession) {
      return;
    }

    if (!$hasSpell) {
      $this->learnSpell($db, $characterId, $profession['spell']);
    }

    if ($hasSkill) {
      $db->update('character_skills')
        ->condition('guid', $characterId)
        ->condition('skill', $profession['skill'])
        ->fields(
          [
            'value' => 300,
            'max' => 300,
          ]
        )
        ->execute();
    }
    else {
      $db->insert('character_skills')
        ->fields(
          [
            'guid' => $characterId,
            'skill' => $profession['skill'],
            'value' => 300,
            'max' => 300,
          ]
        )
        ->execute();
    }
  }

  /**
   * @param $db
   * @param $characterId
   * @param $spell
   */
  public function learnSpell($db, $characterId, $spell) {
    $resultSpell = $db->select('character_spell', 'csp')
      ->fields('csp', ['guid'])
      ->condition('guid', $characterId)
      ->condition('spell', $spell)
      ->execute()
      ->fetchAssoc();

    if (!$resultSpell) {
      $db->insert('character_spell')
        ->fields(
          [
            'guid' => $characterId,
            'spell' => $spell,
            'active' => 1,
            'disabled' => 0,
          ]
        )
        ->execute();
    }
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param array $character
   * @param int $skill
   * @param int $value
   *
   * @throws \Exception
   */
  public function updateSkill($db, $character, $skill, $value = 300) {
    $characterId = $character['guid'];

    $resultSkill = $db->select('character_skills', 'cs')
      ->fields('cs', ['guid'])
      ->condition('guid', $characterId)
      ->condition('skill', $skill)
      ->execute()
      ->fetchAssoc();

    if ($resultSkill) {
      $db->update('character_skills')
        ->condition('guid', $characterId)
        ->condition('skill', $skill)
        ->fields(
          [
            'value' => $value,
            'max' => $value,
          ]
        )
        ->execute();
    }
    else {
      $db->insert('character_skills')
        ->fields(
          [
            'guid' => $characterId,
            'skill' => $skill,
            'value' => $value,
            'max' => $value,
          ]
        )
        ->execute();
    }
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param array $character
   * @param int $skill
   * @param int $value
   *
   * @throws \Exception
   */
  public function hasSkillAndValue($db, $character, $skill, $value = 300) {
    $characterId = $character['guid'];

    $resultSkill = $db->select('character_skills', 'cs')
      ->fields('cs', ['guid'])
      ->condition('guid', $characterId)
      ->condition('skill', $skill)
      ->condition('value', $value)
      ->execute()
      ->fetchAssoc();

    return $resultSkill ? count($resultSkill) : FALSE;
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param array $character
   * @param int $value
   *
   * @throws \Exception
   */
  public function updateSesameSkills($db, $character, $value = 300) {
    $characterId = $character['guid'];
    $skills = $this->getClassSkill($character);

    foreach ($skills as $skill) {
      $resultSkill = $db->select('character_skills', 'cs')
        ->fields('cs', ['guid'])
        ->condition('guid', $characterId)
        ->condition('skill', $skill)
        ->execute()
        ->fetchAssoc();

      if ($resultSkill) {
        $db->update('character_skills')
          ->condition('guid', $characterId)
          ->condition('skill', $skill)
          ->fields(
            [
              'value' => $value,
              'max' => $value,
            ]
          )
          ->execute();
      }
      else {
        $db->insert('character_skills')
          ->fields(
            [
              'guid' => $characterId,
              'skill' => $skill,
              'value' => '290',
              'max' => '290',
            ]
          )
          ->execute();
      }
    }

  }

  /**
   * @param $name
   * @param $price
   * @param $username
   *
   * @return bool
   */
  public function sendMailShop($name, $price, $username) {
    $to = \Drupal::config('system.site')->get('mail');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');
    return $mailManager->mail(
      'warcraft_core',
      'confirm_buy',
      $to,
      $langcode,
      [
        'name' => $name,
        'price' => $price,
        'username' => $username,
      ]
    );
  }

}
