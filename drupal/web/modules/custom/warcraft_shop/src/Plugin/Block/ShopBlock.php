<?php

namespace Drupal\warcraft_shop\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class ShopBlock
 *
 * @Block(
 *   id = "shop_list_block",
 *   admin_label = @Translation("Shop block"),
 *   category = @Translation("Mangos"),
 * )
 */
class ShopBlock extends BlockBase {

  /**
   * @inheritDoc
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * @inheritDoc
   */
  public function build() {
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'product')
      ->condition('status', 1)
      ->execute();

    if (!$nids) {
      return [];
    }

    $nodes = Node::loadMultiple($nids);

    usort($nodes, function($nodeA, $nodeB) {
      $weightA = $nodeA->get('field_product_weight')->value ?: 0;
      $weightB = $nodeB->get('field_product_weight')->value ?: 0;

      return $weightA > $weightB;
    });

    $products = [];
    /** @var \Drupal\warcraft_media\Services\MediaProvider $mediaProvider */
    $mediaProvider = \Drupal::service('warcraft_media.media_provider');
    $products['all'] = [
      'id' => 'all',
      'name' => $this->t('All', [], ['warcraft_shop']),
      'weight' => -1000,
      'products' => array_map(function(NodeInterface $node) use ($mediaProvider) {
        return [
          'id' => $node->id(),
          'name' => $node->getTitle(),
          'category' => 'all',
          'description' => $node->get('field_product_description')->value,
          'image' => $mediaProvider->getMediaUrl(
            $node->get('field_product_image')->entity
          ),
          'price' => $node->get('field_product_price')->value,
          'itemName' => $node->get('field_product_item_name')->value,
          'serviceType' => $node->get('field_service_type')->value,
          'itemId' => $node->get('field_product_item_id')->value,
        ];
      }, $nodes),
    ];

    foreach ($nodes as $node) {
      /** @var \Drupal\taxonomy\Entity\Term $category */
      $category = $node->get('field_product_category')->referencedEntities()[0];
      if (!array_key_exists($category->id(), $products)) {
        $products[$category->id()] = [
          'id' => $category->id(),
          'name' => $category->getName(),
          'weight' => $category->getWeight(),
        ];
      }

      $products[$category->id()]['products'][] = [
        'id' => $node->id(),
        'name' => $node->getTitle(),
        'category' => $category->getName(),
        'description' => $node->get('field_product_description')->value,
        'image' => $mediaProvider->getMediaUrl(
          $node->get('field_product_image')->entity
        ),
        'type' => $node->get('field_product_type')->value,
        'serviceType' => $node->get('field_service_type')->value,
        'price' => $node->get('field_product_price')->value,
        'itemName' => $node->get('field_product_item_name')->value,
        'itemId' => $node->get('field_product_item_id')->value,
      ];
    }

    usort($products, function($categoryA, $categoryB) {
      return $categoryA['weight'] > $categoryB['weight'];
    });

    return [
      '#theme' => 'shop',
      '#isConnected' => !\Drupal::currentUser()->isAnonymous(),
      '#charactersUrl' => Url::fromRoute('warcraft_user.api.characters')
        ->toString(),
      '#buyItemUrl' => Url::fromRoute('warcraft_user.api.buy')
        ->toString(),
      '#products' => $products,
    ];
  }

}
