<?php

namespace Drupal\warcraft_user\Command;

use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;
use Laizerox\Wowemu\SRP\UserClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MigrateAccounts
 *
 * @package Drupal\warcraft_user\Command
 */
class MigrateAccounts extends DrushCommands {

  /**
   * Migrate all accounts
   *
   * @command account:migrate
   * @aliases am
   * @usage account:migrate
   *   Migrate all accounts from json into account Table
   *
   * @throws \Exception
   */
  public function migrateAccounts() {
    $content = file_get_contents('https://les-terres-illidaris.fr/themes/account_mangos.json');
    $content = json_decode($content, TRUE);
    foreach ($content as $item) {
      $username = $item['username'];
      $password = str_shuffle(substr(sha1($username), 0, 10));
      /* Create your v and s values. */
      $client = new UserClient($username);
      $salt = $client->generateSalt();
      $verifier = $client->generateVerifier($password);

      $user = \Drupal::entityQuery('user')
        ->condition('name', $username)
        ->execute();

      if (!$user) {
        $this->output->writeln('User '.$username.' not found in Drupal. The password is :' . $password);
        continue;
      }

      $this->output->writeln('Importing ' . $username);

      $user = User::load(reset($user));

      if (!$user) {
        $this->output->writeln('User '.$username.' not found. The password is :' . $password);
        continue;
      }

      $email = $user->get('mail')->value;

      Database::setActiveConnection('realmd');
      $db = Database::getConnection();
      $db->insert('account')->fields(
        [
          'id' => $item['id'],
          'username' => $username,
          'email' => $email,
          'v' => $verifier,
          's' => $salt,
          'expansion' => 1,
          'joindate' => $item['joindate'],
          'last_ip' => $item['last_ip'],
          'locale' => $item['locale'],
          'last_login' => $item['last_login'],
          'gmlevel' => $item['gmlevel'],
        ]
      )->execute();

      Database::setActiveConnection();

      $user->setPassword($password);
      $user->save();

      /** @var \Drupal\Core\Mail\MailManager $mailManager */
      $mailManager = \Drupal::service('plugin.manager.mail');
      $mailManager->mail(
        'warcraft_user',
        'migration',
        $email,
        'fr',
        [
          'newPassword' => $password,
          'email' => $email,
          'username' => $username,
        ]
      );
    }
  }

}
