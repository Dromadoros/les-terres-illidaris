<?php

namespace Drupal\warcraft_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\jsonapi\JsonApiResource\Data;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;
use Laizerox\Wowemu\SRP\UserClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiUserController
 *
 * @package Drupal\warcraft_user\Controller
 */
class ApiUserController extends ControllerBase {

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function login(Request $request) {
    /** @var \Drupal\Core\Password\PasswordInterface $passwordChecker */
    $passwordChecker = \Drupal::service('password');
    $content = json_decode($request->getContent(), TRUE);
    $email = array_key_exists('email', $content) ? $content['email'] : NULL;
    $password = array_key_exists(
      'password',
      $content
    ) ? $content['password'] : NULL;

    $users = \Drupal::entityQuery('user')
      ->condition('mail', $email)
      ->execute();

    if (empty($users)) {
      return new JsonResponse('', Response::HTTP_BAD_REQUEST);
    }

    /** @var User $user */
    $user = User::load(reset($users));

    if (!$passwordChecker->check($password, $user->getPassword())) {
      return new JsonResponse('', Response::HTTP_BAD_REQUEST);
    }

    user_login_finalize($user);

    return new JsonResponse(
      [
        'url' => Url::fromRoute('warcraft_user.panel')->toString(),
      ]
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updatePassword(Request $request) {
    $content = json_decode($request->getContent(), TRUE);
    $newPassword = array_key_exists(
      'newPassword',
      $content
    ) ? $content['newPassword'] : NULL;
    /** @var User $user */
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    if (!$user instanceof User || $user->isAnonymous()) {
      return new JsonResponse('User not connected', Response::HTTP_NO_CONTENT);
    }

    if (!$newPassword) {
      return new JsonResponse(
        'No response received', Response::HTTP_NO_CONTENT
      );
    }
    /* Create your v and s values. */
    $client = new UserClient($user->getAccountName());
    $salt = $client->generateSalt();
    $verifier = $client->generateVerifier($newPassword);

    Database::setActiveConnection('realmd');
    $db = Database::getConnection();
    $db->update('account')->fields(
      [
        'sessionkey' => '',
        'v' => $verifier,
        's' => $salt,
      ]
    )->condition('username', $user->getUsername())->execute();

    Database::setActiveConnection();

    $user->setPassword($newPassword);
    $user->save();

    return new JsonResponse();
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function characters(Request $request) {
    $userId = \Drupal::currentUser()->id();
    $loginUrl = Url::fromRoute('warcraft_user.login')->toString();

    if (!$userId) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You are not connected, please <a href="@loginUrl">login</a>',
            ['@loginUrl' => $loginUrl],
            ['context' => 'warcraft_user']
          ),
        ],
      );
    }

    $user = \Drupal\user\Entity\User::load($userId);

    if (!$user) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You are not connected, please <a href="@loginUrl">login</a>',
            ['@loginUrl' => $loginUrl],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ],
      );
    }

    Database::setActiveConnection('realmd');
    $db = Database::getConnection();

    $results = $db
      ->query(
        'SELECT id FROM account WHERE account.username= \'' . $user->getUsername(
        ) . '\''
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!$results) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You don\'t have characters yet',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ],
      );
    }

    $id = reset($results)['id'];

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $characters = $db->query(
      'SELECT * FROM characters WHERE characters.account = \'' . $id . '\''
    )->fetchAll(\PDO::FETCH_ASSOC);

    $data = [];

    foreach ($characters as $character) {
      $data[$character['guid']] = [
        'name' => $character['name'],
        'id' => $character['guid'],
        'class' => $character['class'],
      ];
    }

    return new JsonResponse(
      [
        'error' => FALSE,
        'characters' => $data,
        'spe' => [
          1 => [
            'war_arme' => [
              'label' => 'Guerrier Arme',
            ],
            'war_fury' => [
              'label' => 'Guerrier Fury',
            ],
            'war_tank' => [
              'label' => 'Guerrier Protection',
            ],
          ],
          2 => [
            'paladin_vindi' => [
              'label' => 'Paladin Vindicte',
            ],
            'paladin_tank' => [
              'label' => 'Paladin Protect',
            ],
            'paladin_heal' => [
              'label' => 'Paladin Heal',
            ],
          ],
          3 => [
            'hunt_beast' => [
              'label' => 'Chasseur Bête',
            ],
            'hunt_survival' => [
              'label' => 'Chasseur Survie',
            ],
            'hunt_preci' => [
              'label' => 'Chasseur Précision',
            ],
          ],
          4 => [
            'rogue_finesse' => [
              'label' => 'Voleur Finesse',
            ],
            'rogue_combat' => [
              'label' => 'Voleur Combat',
            ],
            'rogue_assa' => [
              'label' => 'Voleur Assa',
            ],
          ],
          5 => [
            'priest_shadow' => [
              'label' => 'Prêtre Ombre',
            ],
            'priest_disci' => [
              'label' => 'Prêtre Disci',
            ],
            'priest_sacry' => [
              'label' => 'Prêtre sacré',
            ],
          ],
          7 => [
            'shaman_elem' => [
              'label' => 'Chaman Elem',
            ],
            'shaman_ambi' => [
              'label' => 'Chaman Amélio',
            ],
            'shaman_heal' => [
              'label' => 'Chaman Heal',
            ],
          ],
          8 => [
            'mage_givre' => [
              'label' => 'Mage Givre',
            ],
            'mage_fire' => [
              'label' => 'Mage Feu',
            ],
            'mage_arcane' => [
              'label' => 'Mage Arcane',
            ],
          ],
          9 => [
            'warlock_affli' => [
              'label' => 'Démoniste Affli',
            ],
            'warlock_demono' => [
              'label' => 'Démoniste Démonologie',
            ],
            'warlock_destru' => [
              'label' => 'Démoniste Destru',
            ],
          ],
          11 => [
            'druid_feral' => [
              'label' => 'Druide Feral',
            ],
            'druid_poule' => [
              'label' => 'Druide Equilibre',
            ],
            'druid_heal' => [
              'label' => 'Druide Heal',
            ],
            'druid_tank' => [
              'label' => 'Druide Tank',
            ],
          ],
        ],
        'title' => t('Choose a character', [], ['context' => 'warcraft_user']),
      ]
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function buy(Request $request) {
    if (!$this->isServerUp()) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'Le serveur est momentanément indisponible, veuillez réessayer plus tard',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    $content = $request->getContent();
    $content = json_decode($content, TRUE);
    $characterId = $content['character'];
    $currentSpe = $content['spe'];
    $productId = $content['productId'];
    $userId = \Drupal::currentUser()->id();
    $user = \Drupal\user\Entity\User::load($userId);

    if (!$user) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You are not connected, please <a href="@loginUrl">login</a>',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    $node = Node::load($productId);
    /** @var \Drupal\warcraft_shop\Service\ShopManager $shopManager */
    $shopManager = \Drupal::service('warcraft_shop.shop_manager');

    if (!$node instanceof NodeInterface) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'This product doesn\'t exists anymore',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    $cost = $node->get('field_product_price')->value;
    $userPoints = $user->get('field_user_points')->value;

    if ($userPoints < $cost) {
      return new JsonResponse(
        [
          'error' => TRUE,
          'message' => t(
            'You do not have enough points.',
            [],
            ['context' => 'warcraft_user']
          ),
          'title' => t('Error', [], ['context' => 'warcraft_user']),
        ]
      );
    }

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $character = $db->select('characters', 'c')
      ->fields(
        'c',
        ['guid', 'level', 'name', 'race', 'class', 'totaltime', 'online']
      )
      ->condition(
        'guid',
        $characterId
      )
      ->execute()
      ->fetchAssoc();
    $characterName = $character['name'];

    // If the product is a service we return directly in the if
    if ('service' === $node->get('field_product_type')->value) {
      $serviceType = $node->get('field_service_type')->value;
      switch ($serviceType) {
        case 'switchName':
          return $shopManager->switchName($db, $character, $user, $cost, $node);
          break;
        case 'boost':
          return $shopManager->boost($db, $character, $currentSpe, $user, $cost, $node);
          break;
        case 'leveling':
          return $shopManager->sesame($db, $character, $user, $cost, $node);
          break;
        case 'mount':
          return $shopManager->mount($db, $character, $user, $cost, $node);
          break;
        case 'fly':
          return $shopManager->fly($db, $character, $user, $cost, $node);
          break;
        default:
          return new JsonResponse(
            [
              'error' => TRUE,
              'title' => t('Error', [], ['context' => 'warcraft_user']),
              'message' => t(
                'Le produit est mal configuré, veuillez contacter un membre du staff.',
                ['@name' => $node->get('field_product_item_name')->value],
                ['context' => 'warcraft_user']
              ),
            ]
          );
          break;
      }
    }

    if ('profession' === $node->get('field_product_type')->value) {
      $content = $shopManager->buyProfession(
        $db,
        $characterId,
        $node->get('field_product_profession')->value
      );
      $decodedContent = json_decode($content);

      if ($decodedContent['error']) {
        return $content;
      }

      $this->userPay($user, $node, $cost, $userPoints);
      $shopManager->sendMailShop(
        $node->getTitle(),
        $cost,
        $user->getUsername()
      );

      return $content;
    }

    $itemId = $node->get('field_product_item_id')->value;
    $mailTitle = t(
      '[BOUTIQUE] Congratulations for your new item !',
      [],
      ['context' => 'warcraft_user']
    );
    $mailItem = "$itemId:1";
    $command = ".send items " . $characterName . " \"" . $mailTitle . "\" \"\" " . $mailItem;
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $this->userPay($user, $node, $cost, $userPoints);

    $shopManager->sendMailShop($node->getTitle(), $cost, $user->getUsername());

    return new JsonResponse(
      [
        'error' => FALSE,
        'title' => t('Congratulations !', [], ['context' => 'warcraft_user']),
        'message' => t(
          'You will receive your @name soon in mailbox.',
          ['@name' => $node->get('field_product_item_name')->value],
          ['context' => 'warcraft_user']
        ),
      ]
    );
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $user
   * @param NodeInterface $node
   * @param $cost
   * @param $userPoints
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function userPay($user, $node, $cost, $userPoints) {
    $user->set('field_user_points', $userPoints - $cost);
    $user->save();

    $sells = $node->get('field_product_sells')->value ?: 0;
    $node->set('field_product_sells', $sells + 1);
    $node->save();
  }

  /**
   * @return bool
   */
  private function isServerUp() {
    $server = Settings::get('server_url');
    return FALSE !== fsockopen($server, 3724) && FALSE !== fsockopen(
        $server,
        8085
      );
  }

}
