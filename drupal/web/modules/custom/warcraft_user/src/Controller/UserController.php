<?php

namespace Drupal\warcraft_user\Controller;

use Drupal\block\Entity\Block;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 *
 * @package Drupal\warcraft_user\Controller
 */
class UserController extends ControllerBase {

  const VOTES = [
    'rpg' => [
      'url' => 'https://www.rpg-paradize.com/?page=vote&vote=112530',
      'field' => 'field_user_last_voted_rpg',
      'seconds' => 10800,
    ],
    'serveur-prive' => [
      'url' => 'https://serveur-prive.net/world-of-warcraft/les-terres-illidaris-2-4-3-2610/vote',
      'field' => 'field_user_last_voted',
      'seconds' => 5400,
    ],
    'topg' => [
      'url' => 'https://topg.org/fr/wow-private-servers/in-592383',
      'field' => 'field_user_last_voted_topg',
      'seconds' => 10800,
    ],
  ];

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function login(Request $request) {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    if ($user instanceof User && !$user->isAnonymous()) {
      return $this->redirect('warcraft_user.panel');
    }

    return [
      '#theme' => 'user_login',
      '#loginApiUrl' => Url::fromRoute('warcraft_user.api.login')->toString(),
    ];
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function loginTitle() {
    return $this->t('Log in', [], ['context' => 'warcraft_user']);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function panel(Request $request) {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    if (!$user instanceof User || $user->isAnonymous()) {
      return $this->redirect('warcraft_user.login');
    }

    /** @var \Drupal\Core\Block\BlockManager $blockManager */
    $blockManager = \Drupal::service('plugin.manager.block');
    // You can hard code configuration or you load from settings.
    $pluginBlock = $blockManager->createInstance('hardcore_recuperation_block');
    $blockRecup = $pluginBlock->build();

    return [
      '#theme' => 'user_panel',
      '#blockRecup' => $blockRecup,
      '#userData' => [
        'email' => $user->getEmail(),
        'username' => $user->getUsername(),
        'points' => $user->get('field_user_points')->value,
        'updateApiUrl' => Url::fromRoute('warcraft_user.api.update_password')
          ->toString(),
        'disconnectUrl' => Url::fromRoute('warcraft_user.disconnect')->toString(
        ),
      ],
    ];
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $site
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function vote(Request $request, $site) {
    $vote = array_key_exists($site, self::VOTES) ? self::VOTES[$site] : NULL;

    if (!$vote) {
      return new RedirectResponse('/');
    }

    /** @var User $user */
    $user = User::load(\Drupal::currentUser()->id());

    if (!$user || $user->isAnonymous()) {
      \Drupal::messenger()->addMessage(
        $this->t(
          'Vous devez être connecté afin de voter',
          [],
          ['context' => 'warcraft_user']
        )
      );

      header('Location: ' . $vote['url']);
      die;
    }

    $timeDifference = time() - $user->get($vote['field'])->value;

    if ($timeDifference < $vote['seconds']) {
      \Drupal::messenger()->addMessage(
        $this->t(
          'You cannot vote yet, try again later ...',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return new RedirectResponse('/');
    }

    $points = $user->get('field_user_points')->value ?: 0;
    $votes = $user->get('field_user_votes')->value ?: 0;
    $points += 1;
    $votes += 1;

    $user->set('field_user_points', $points);
    $user->set('field_user_votes', $votes);
    $user->set($vote['field'], time());
    $user->save();

    header('Location: ' . $vote['url']);

    \Drupal::messenger()->addMessage(
      $this->t(
        'Vote registred, thank you. One point is added to your account. You have now @points points',
        ['@points' => $points],
        ['context' => 'warcraft_core']
      )
    );
    die;

    return new RedirectResponse('/');
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function panelTitle() {
    return $this->t('My account', [], ['context' => 'warcraft_user']);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function disconnect(Request $request) {
    user_logout();

    return $this->redirect('warcraft_user.login');
  }

}
