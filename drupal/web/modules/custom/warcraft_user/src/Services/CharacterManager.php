<?php

namespace Drupal\warcraft_user\Services;

use Drupal\Core\Database\Database;

/**
 * Class CharacterManager
 *
 * @package Drupal\warcraft_user\Services
 */
class CharacterManager {

  /**
   * @param int $requiredLevel
   * @param int $maxLevel
   * @param string $characterDb
   *
   * @return array
   */
  public function getUserCharacters(int $requiredLevel = 1, int $maxLevel = 70, string $characterDb = 'character') {
    $id = $this->getAccountId();

    Database::setActiveConnection($characterDb);
    $db = Database::getConnection();

    return $db->query(
      'SELECT * FROM characters WHERE characters.account = \'' . $id . '\' AND level >= \'' . $requiredLevel . '\' AND level <= \'' . $maxLevel . '\''
    )->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * @param int $characterId
   *
   * @return array
   */
  public function getCharacterData(int $characterId) {
    Database::setActiveConnection('character');
    $db = Database::getConnection();

    $character = $db->select('characters', 'c')
      ->fields('c', ['guid', 'level', 'name', 'race', 'class', 'totaltime', 'online'])
      ->condition(
        'guid',
        $characterId
      )
      ->execute()
      ->fetchAssoc();

    return $character;
  }

  /**
   * @return int
   */
  public function getAccountId() {
    $userId = \Drupal::currentUser()->id();

    if (!$userId) {
      return 0;
    }

    $user = \Drupal\user\Entity\User::load($userId);

    if (!$user) {
      return 0;
    }

    Database::setActiveConnection('realmd');
    $db = Database::getConnection();

    $results = $db
      ->query(
        'SELECT id FROM account WHERE account.username= \'' . $user->getUsername(
        ) . '\''
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!$results) {
      return 0;
    }

    return reset($results)['id'];
  }

}
