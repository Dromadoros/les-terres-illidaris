<?php

/**
 * @file
 * Contains paypal_payment.page.inc.
 *
 * Page callback for Paypal payment entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Paypal payment templates.
 *
 * Default template: paypal_payment.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_paypal_payment(array &$variables) {
  // Fetch PaypalPayment Entity Object.
  $paypal_payment = $variables['elements']['#paypal_payment'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
