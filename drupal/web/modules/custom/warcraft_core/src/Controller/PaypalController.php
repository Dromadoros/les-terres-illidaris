<?php

namespace Drupal\warcraft_core\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\warcraft_core\Constants\Paypal;
use Drupal\warcraft_core\Entity\PaypalPayment;
use GuzzleHttp\RequestOptions;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentInstruction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaypalController
 *
 * @package Drupal\warcraft_core\Controller
 */
class PaypalController extends ControllerBase {

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param int $amount
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function pay(Request $request, int $amount) {
    //Need to check if the amount is 5,10 or 20 otherwise return.
    if (!in_array($amount, Paypal::AMOUNTS)) {
      \Drupal::messenger()->addError(
        $this->t(
          'You tried to insert an incorrect amount',
          [],
          ['context' => 'ucm_core']
        )
      );

      return $this->redirect('warcraft_settings.homepage');
    }

    $isConnected = !\Drupal::currentUser()->isAnonymous();

    if (!$isConnected) {
      \Drupal::messenger()->addError(
        $this->t(
          'You are not connected',
          [],
          ['context' => 'ucm_core']
        )
      );

      return $this->redirect('warcraft_settings.homepage');
    }

    $apiContext = new ApiContext(
      new OAuthTokenCredential(
        Settings::get('paypal_client_id'),     // ClientID
        Settings::get('paypal_client_secret')      // ClientSecret
      )
    );

    $apiContext->setConfig(
      [
        'mode' => 'live',
      ]
    );

    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

    $amountPaypal = new Amount();
    $amountPaypal->setTotal((float) $amount);
    $amountPaypal->setCurrency('EUR');

    $transaction = new Transaction();
    $transaction->setAmount($amountPaypal);

    $redirectUrls = new RedirectUrls();
    $baseUrl = \Drupal::request()->getSchemeAndHttpHost();
    $uriSuccess = Url::fromRoute('warcraft_core.paypal_success')->toString();
    $uriFail = Url::fromRoute('warcraft_core.paypal_fail')->toString();
    $redirectUrls->setReturnUrl($baseUrl . $uriSuccess)
      ->setCancelUrl($baseUrl . $uriFail);

    $payment = new Payment();
    $payment->setIntent('sale')
      ->setPayer($payer)
      ->setTransactions([$transaction])
      ->setRedirectUrls($redirectUrls);

    try {
      $payment->create($apiContext);
      $url = Url::fromUri($payment->getApprovalLink(), ['external' => TRUE]);
      // What the fuck Drupal, seriously.
      // @see
      //   https://www.drupal.org/node/2630808
      //   https://drupal.stackexchange.com/questions/225956/cache-controller-with-json-response
      //   ... and many others.
      header('Location: ' . $url->toString(TRUE)->getGeneratedUrl());
      die;
    } catch (PayPalConnectionException $ex) {
      \Drupal::logger('paypal')->error($ex->getData());
    }

    \Drupal::messenger()->addError(
      $this->t(
        'Something went wront with the payment',
        [],
        ['context' => 'ucm_core']
      )
    );

    return new RedirectResponse($baseUrl);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function success(Request $request) {
    /** @var \Drupal\Core\Session\AccountProxyInterface $user */
    $user = \Drupal::currentUser();

    if ($user->isAnonymous()) {
      return $this->returnToPageWithError(
        'You are not connected',
        'You are not connected'
      );
    }

    $apiContext = new ApiContext(
      new OAuthTokenCredential(
        Settings::get('paypal_client_id'),     // ClientID
        Settings::get('paypal_client_secret')      // ClientSecret
      )
    );

    $apiContext->setConfig(
      [
        'mode' => 'live',
      ]
    );

    $paymentId = $request->get('paymentId');
    $payerId = $request->get('PayerID');

    if (!$paymentId) {
      return $this->returnToPageWithError(
        'No payment ID in request',
        'We didn\'t received your payment'
      );
    }

    try {
      $payment = Payment::get($paymentId, $apiContext);

      $paymentResults = \Drupal::entityQuery('paypal_payment')->condition(
        'field_paypal_payment_id',
        $paymentId
      )->execute();

      if (!empty($paymentResults)) {
        return $this->returnToPageWithError(
          'We already found a payment with this ID',
          'You already received points'
        );
      }

      if (empty($payment->getTransactions())) {
        return $this->returnToPageWithError(
          'No transactions found',
          'No transactions found'
        );
      }

      $amount = $payment->getTransactions()[0]->getAmount()->getTotal();

      $paymentEntity = PaypalPayment::create(
        [
          'name' => 'P: ' . $user->getDisplayName() . ' - ' . $amount,
          'field_paypal_payment_id' => $paymentId,
          'field_paypal_payment_amount' => $amount,
          'field_paypal_payment_user_id' => $user->id(),
        ]
      );

      $paymentEntity->save();

      /** @var \Drupal\user\UserInterface $user */
      $user = User::load($user->id());
      $winPoints = 0;

      switch ((int) $amount) {
        case 5;
          $winPoints = 50;
          break;
        case 10;
          $winPoints = 115;
          break;
        case 20;
          $winPoints = 250;
          break;
      }

      $points = $user->get('field_user_points')->value ?: 0;
      $points += $winPoints;

      $user->set('field_user_points', $points);
      $user->save();

    } catch (\Exception $ex) {
      return $this->returnToPageWithError(
        $ex,
        'Something went wrong with the payment'
      );
    }

    \Drupal::messenger()->addMessage(
      $this->t(
        'Thank you for your support !',
        [],
        ['context' => 'ucm_core']
      )
    );

    $client = \Drupal::httpClient();

    $response = $client->post(
      'https://api.paypal.com/v1/oauth2/token',
      [
        RequestOptions::HEADERS => [
          'Authorization' => 'Basic ' . base64_encode(
              Settings::get('paypal_api_username') . ':' . Settings::get('paypal_api_password')
            ),
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'form_params' => [
          'grant_type' => 'client_credentials',
        ],
      ]
    );

    $data = json_decode($response->getBody()->getContents(), TRUE);
    $accessToken = $data['access_token'];

    $client->post(
      'https://api.paypal.com/v1/payments/payment/' . $paymentId . '/execute',
      [
        RequestOptions::HEADERS => [
          'Authorization' => 'Bearer ' . $accessToken,
          'Content-Type' => 'application/json',
        ],
        RequestOptions::JSON => [
          'payer_id' => $payerId,
        ],
      ]
    );

    return $this->redirect('warcraft_settings.homepage');
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function fail(Request $request) {
    return $this->returnToPageWithError(
      'Paypal return to fail page',
      'You cancelled your payment'
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function successAllopass(Request $request) {
    /** @var \Drupal\Core\Session\AccountProxyInterface $user */
    $user = \Drupal::currentUser();

    if ($user->isAnonymous()) {
      return $this->returnToPageWithError(
        'You are not connected',
        'You are not connected'
      );
    }

    $refererLink = $request->headers->get('referer');
    $position = strpos($refererLink, Settings::get('allopass_url'));
    $request->headers->set('referer', '');

    if ($position !== 0) {
      return $this->returnToPageWithError(
        'Allopass - error',
        'Votre paiement a été annulé par Allopass'
      );
    }

    $paymentEntity = PaypalPayment::create(
      [
        'name' => 'A: ' . $user->getDisplayName() . ' - ' . '3€',
        'field_paypal_payment_id' => 'Allopass',
        'field_paypal_payment_amount' => 3,
        'field_paypal_payment_user_id' => $user->id(),
      ]
    );

    $paymentEntity->save();

    /** @var \Drupal\user\UserInterface $user */
    $user = User::load($user->id());
    $winPoints = 25;

    $points = $user->get('field_user_points')->value ?: 0;
    $points += $winPoints;

    $user->set('field_user_points', $points);
    $user->save();

    \Drupal::messenger()->addMessage(
      $this->t(
        'Thank you for your support !',
        [],
        ['context' => 'ucm_core']
      )
    );

    return $this->redirect('warcraft_settings.homepage');
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function failAllopass(Request $request) {
    return $this->returnToPageWithError(
      'Allopass - error',
      'Votre paiement a été annulé par Allopass'
    );
  }

  /**
   * @param string $logError
   * @param string $messageError
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  private function returnToPageWithError(
    string $logError,
    string $messageError
  ) {
    \Drupal::logger('paypal')->error($logError);
    \Drupal::messenger()->addError(
      $this->t(
        $messageError,
        [],
        ['context' => 'warcraft_core']
      )
    );

    return $this->redirect('warcraft_settings.homepage');
  }

}
