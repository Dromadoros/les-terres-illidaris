<?php

namespace Drupal\warcraft_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

/**
 * Class TicketsController
 *
 * @package Drupal\warcraft_core\Controller
 */
class TicketsController extends ControllerBase {

  /**
   * @return array
   */
  public function tickets() {
    Database::setActiveConnection('character');
    $db = Database::getConnection();


    $tickets = $db
      ->query(
        'SELECT 
    *
FROM
    gm_tickets
WHERE
    gm_tickets.state = 0;'
      )
      ->fetchAll(\PDO::FETCH_ASSOC);

    return [
      '#theme' => 'tickets',
      '#tickets' => $tickets,
    ];
  }

}
