<?php

namespace Drupal\warcraft_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class HomepageController
 *
 * @package Drupal\warcraft_core\Controller
 */
class HomepageController extends ControllerBase {

  /**
   * @return array
   */
  public function home() {
    $nid = \Drupal::config('warcraft_core.settings')->get('homepage');

    if (!$nid) {
      throw new NotFoundHttpException();
    }

    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $node = Node::load($nid);
    $build = $viewBuilder->view($node);
    $metaTags = metatag_get_tags_from_route($node);

    $fid = $this->config('warcraft_core.settings')->get('header_file');
    $file = \Drupal\file\Entity\File::load(reset($fid));

    $url = $file ? $file->getFileUri() : NULL;

    return [
      '#theme' => 'homepage',
      '#content' => render($build),
      $metaTags,
      '#headerUrl' => $url,
    ];
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function homepageTitle() {
    return $this->t('Mangos test', [], ['context' => 'warcraft_core']);
  }
}
