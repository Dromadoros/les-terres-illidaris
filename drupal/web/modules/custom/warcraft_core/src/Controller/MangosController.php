<?php

namespace Drupal\warcraft_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MangosController
 *
 * @package Drupal\warcraft_core\ControllerS
 */
class MangosController extends ControllerBase {

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function version() {
    $config = \Drupal::config('warcraft_core.settings');

    return new JsonResponse($config->get('version'));
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function sendMessage(Request $request) {
    $content = json_decode($request->getContent(), TRUE);

    if (empty($content)) {
      return new JsonResponse([], Response::HTTP_BAD_REQUEST);
    }

    $username = array_key_exists('username', $content) ? $content['username'] : NULL;
    $message = array_key_exists('message', $content) ? $content['message'] : NULL;

    if (!$message || !$username) {
      return new JsonResponse([], Response::HTTP_BAD_REQUEST);
    }

    if (strpos($message, 'Les Terres Illidaris - World') !== FALSE) {
      return new JsonResponse([], Response::HTTP_BAD_REQUEST);
    }

    $command = ".discord |cFFC0C0C0[world][|cFF9B59B6D|r|cFFC0C0C0:$username] : $message";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    return new JsonResponse([]);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function special() {
    $config = \Drupal::config('warcraft_core.settings');

    return new JsonResponse($config->get('special'));
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function instances() {
    $now = new DrupalDateTime('now');

    $instances = \Drupal::entityQuery('node')
      ->condition('type', 'instance_pool')
      ->condition('status', Node::PUBLISHED)
      ->condition(
        'field_instance_date',
        $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '>='
      )
      ->sort('field_instance_date', 'ASC')
      ->execute();

    if (empty($instances)) {
      return new JsonResponse([], Response::HTTP_BAD_REQUEST);
    }

    /** @var \Drupal\node\NodeInterface $node */
    $nodes = Node::loadMultiple($instances);

    $values = array_map(
      function (Node $node) {
        /** @var \Drupal\node\NodeInterface $instance */
        $instance = $node->get('field_instance_reference')->entity;
        $date = $node->get('field_instance_date')->value;

        $date = new \DateTime($date);

        return [
          'title' => $instance->getTitle(),
          'url' => $node->toUrl()->toString(),
          'date' => $date->format('d/m/Y - H:i'),
        ];
      },
      $nodes
    );

    return new JsonResponse(array_values($values));
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getUniqueNews(Request $request) {
    $index = $request->get('index') ?: 0;

    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'news')
      ->condition('status', 1)
      ->sort('field_news_published_date', 'DESC')
      ->range(0, 5)
      ->execute();

    if (!$nids) {
      return new JsonResponse();
    }


    $nodes = array_values(Node::loadMultiple($nids));
    usort($nodes, function(Node $a, Node $b) {
      $dateA = new \DateTime($a->get('field_news_published_date')->value);
      $dateB = new \DateTime($b->get('field_news_published_date')->value);

      return ($dateB->getTimestamp() + $a->id()) <=> ($dateA->getTimestamp() + $b->id());
    });

    /** @var \Drupal\node\NodeInterface $node */
    $node = $nodes[$index];
    /** @var \Drupal\warcraft_media\Services\MediaProvider $mediaProvider */
    $mediaProvider = \Drupal::service('warcraft_media.media_provider');

    return new JsonResponse(
      [
        'title' => $node->getTitle(),
        'description' => $node->get('field_news_teaser')->value,
        'image' => $mediaProvider->getMediaUrl(
          $node->get('field_news_teaser_image')->entity
        ),
        'url' => $node->toUrl()->toString(),
      ]
    );
  }

}
