<?php

namespace Drupal\warcraft_core\Elastic;

use Drupal\Core\Site\Settings;
use Elasticsearch\ClientBuilder;

/**
 * Class Elastic
 *
 * @package Drupal\custom\warcraft_core\Elastic
 */
class Elastic {

  /**
   * @var \Elasticsearch\Client
   */
  private $client;

  /**
   * Elastic constructor.
   */
  public function __construct() {
    $client = ClientBuilder::create();
    $client->setHosts([Settings::get('elastic_host')]);
    $client->setBasicAuthentication(Settings::get('elastic_user'), Settings::get('elastic_password'));

    $this->client = $client->build();
  }

  /**
   * @param int $id
   *
   * @return array
   */
  public function getItem(int $id): ?array {
    $data = $this->client->getSource([
      'index' => 'world',
      'type' => 'items',
      'id' => $id
    ]);

    return $data;
  }

}
