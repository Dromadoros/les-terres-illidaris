<?php

namespace Drupal\warcraft_core;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Paypal payment entities.
 *
 * @ingroup warcraft_core
 */
class PaypalPaymentListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Paypal payment ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\warcraft_core\Entity\PaypalPayment $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.paypal_payment.edit_form',
      ['paypal_payment' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
