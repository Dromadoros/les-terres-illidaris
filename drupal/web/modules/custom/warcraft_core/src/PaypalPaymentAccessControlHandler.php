<?php

namespace Drupal\warcraft_core;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Paypal payment entity.
 *
 * @see \Drupal\warcraft_core\Entity\PaypalPayment.
 */
class PaypalPaymentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\warcraft_core\Entity\PaypalPaymentInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished paypal payment entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published paypal payment entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit paypal payment entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete paypal payment entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add paypal payment entities');
  }


}
