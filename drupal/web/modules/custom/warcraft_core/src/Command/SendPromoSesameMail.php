<?php

namespace Drupal\warcraft_core\Command;

use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class SendPromoSesameMail
 *
 * @package Drupal\warcraft_core\Command
 */
class SendPromoSesameMail extends DrushCommands {

  /**
   * Reset votes
   *
   * @command user:send-promo-sesame
   * @aliases u:sp
   * @usage user:send-promo-sesame
   *   Send promo sesame to all users
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function sendPromoSesame() {
    $users = \Drupal::entityQuery('user')->execute();
    $users = User::loadMultiple($users);
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var User $user */
    foreach ($users as $user) {
        $points = $user->get('field_user_points')->value ?: 0;
        $points += 50;
        $this->output->writeln('Sending mail to ' . $user->getEmail());

        $user->set('field_user_points', $points);
        $user->save();

        if ($user->getEmail()) {
            /** @var \Drupal\Core\Mail\MailManager $mailManager */
            $mailManager = \Drupal::service('plugin.manager.mail');
            $mailManager->mail('warcraft_core', 'promo_sesame_mail', $user->getEmail(), $langcode);
        }
    }
  }

}
