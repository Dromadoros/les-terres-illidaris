<?php

namespace Drupal\warcraft_core\Command;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class Giveaway Parrain
 *
 * @package Drupal\warcraft_core\Command
 */
class GiveawayParrain extends DrushCommands {

  const GIVEAWAY_POINTS = 70;

  /**
   * Giveaway for all parrains that get 70
   *
   * @command user:giveaway-parrain
   * @aliases u:gp
   * @usage user:giveaway-parrain
   *   Giveaway parrain
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function giveaway() {
    $users = \Drupal::entityQuery('user')
      ->condition('field_user_parrain_email', '', '!=')
      ->condition('field_user_has_parrain_gift', FALSE)
      ->execute();

    $users = User::loadMultiple($users);
    Database::setActiveConnection('realmd');
    $db = Database::getConnection();

    foreach ($users as $user) {
      $parrain = $user->get('field_user_parrain_email')->value;
      $mail = $user->get('mail')->value;

      $accounts = $db->query("SELECT 
    *
FROM
    tbcrealmd.account
WHERE
    email = '$parrain'
    OR 
    email = '$mail'
    ")
      ->fetchAll(\PDO::FETCH_ASSOC);

      if (empty($accounts) || !$this->validateCharactersLevel($accounts)) {
        $this->output->writeln('No accounts found or characters not yet 70');
        continue;
      }

      Database::setActiveConnection();

      $userParrain = \Drupal::entityQuery('user')
        ->condition('mail', $parrain)
        ->execute();

      if (empty($userParrain)) {
        $this->output->writeln('We didnt found the drupal user of parrain');
        continue;
      }

      $userParrain = User::load(reset($userParrain));
      $this->setGiveawayDrupalUser($userParrain);
      $this->setGiveawayDrupalUser($user);

      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      /** @var \Drupal\Core\Mail\MailManager $mailManager */
      $mailManager = \Drupal::service('plugin.manager.mail');
      $mailManager->mail('warcraft_core', 'giveaway_parrain', $user->getEmail(), $langcode);
      $mailManager->mail('warcraft_core', 'giveaway_parrain', $userParrain->getEmail(), $langcode);

      \Drupal::logger('giveaway_parrain')->info($user->get('mail')->value . ' and '. $parrain .' has received giveaway !');
      $this->output->writeln($user->get('mail')->value . ' and '. $parrain .' has received giveaway !');
    }
  }

  private function validateCharactersLevel($accounts) {
    foreach ($accounts as $account) {
      Database::setActiveConnection('characters');
      $db = Database::getConnection();
      $accountId = $account['id'];

      $characters = $db->query("SELECT 
    *
FROM
    tbccharacters.characters
WHERE
    account = $accountId")
        ->fetchAll(\PDO::FETCH_ASSOC);

      $characters = array_filter($characters, function($character) {
        return (int) $character['level'] === 70;
      });

      if (empty($characters)) {
        $this->output->writeln('Characters not yet 70 !');

        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * @param \Drupal\user\Entity\User $user
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function setGiveawayDrupalUser(EntityInterface $user) {
    $points = $user->get('field_user_points')->value ?: 0;
    $points += self::GIVEAWAY_POINTS;
    $user->set('field_user_has_parrain_gift', TRUE);
    $user->set('field_user_points', $points);
    $user->save();
  }

}
