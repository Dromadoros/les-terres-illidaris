<?php

namespace Drupal\warcraft_core\Command;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class SetSpawntimeTrack
 *
 * @package Drupal\warcraft_core\Command
 */
class SetSpawntimeTrack extends DrushCommands {

  /**
   * Set default votes points to user by their monnaie
   *
   *
   * @command track:set-spawntime
   * @aliases t:sp
   * @usage track:set-spawntime
   *   Set spawntime for track
   */
  public function setTrackSpawntime() {
    $this->setSpawntime(999995);
    $this->setSpawntime(999996);
    $this->setSpawntime(999997);
    $this->setSpawntime(999998);
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $entry
   */
  private function setSpawntime($entry) {
    Database::setActiveConnection('mangos');
    $db = Database::getConnection();

    $db->update('creature')
      ->condition('id', $entry)
      ->fields(
        [
          'spawntimesecsmin' => 600000000,
          'spawntimesecsmax' => 600000000,
          'spawnMask' => 15,
        ]
      )->execute();
  }

}
