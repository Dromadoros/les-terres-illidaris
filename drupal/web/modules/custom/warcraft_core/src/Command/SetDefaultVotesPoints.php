<?php

namespace Drupal\warcraft_core\Command;

use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class SetDefaultVotesPoints
 *
 * @package Drupal\warcraft_core\Command
 */
class SetDefaultVotesPoints extends DrushCommands {

  /**
   * Set default votes points to user by their monnaie
   *
   *
   * @command user:set-default-votes
   * @aliases u:sv
   * @usage user:set-default-votes
   *   Set user default votes
   */
  public function setVotes() {
    $users = \Drupal::entityQuery('user')->execute();
    $users = User::loadMultiple($users);

    /** @var User $user */
    foreach ($users as $user) {
      $user->set('field_user_votes', $user->get('field_user_points')->value ?: 0);
      $user->save();
    }
  }

}
