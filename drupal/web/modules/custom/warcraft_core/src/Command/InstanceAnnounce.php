<?php

namespace Drupal\warcraft_core\Command;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

/**
 * Class InstanceAnnounce
 *
 * @package Drupal\warcraft_core\Command
 */
class InstanceAnnounce extends DrushCommands {

  /**
   * Announce instance soon IG
   *
   * @command user:instance-announce
   * @aliases u:ia
   * @usage user:instance-announce
   *   Announce instance soon IG
   */
  public function instanceAnnounce() {
    $now = new DrupalDateTime('now');

    $instances = \Drupal::entityQuery('node')
      ->condition('type', 'instance_pool')
      ->condition('status', Node::PUBLISHED)
      ->condition(
        'field_instance_date',
        $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '>='
      )
      ->sort('field_instance_date', 'ASC')
      ->execute();

    if (empty($instances)) {
      return;
    }

    $instances = Node::loadMultiple($instances);

    $command = ".announce " . t(
        'Les instances à venir : ',
        [],
        ['context' => 'warcraft_core']
      );
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
    $command = ".announce -------------------------------------------------";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    foreach ($instances as $node) {
      /** @var \Drupal\node\NodeInterface $instance */
      $instance = $node->get('field_instance_reference')->entity;

      if (!$instance && $instance->getType() !== 'instance') {
        continue;
      }

      $date = $node->field_instance_date->date->format('d/m/Y H:i');

      $command = ".announce " . $date . " - " . $instance->getTitle();
      shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
    }


    $command = ".announce -------------------------------------------------";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
    $command = ".announce " . t(
        'Inscriptions instances : https://les-terres-illidaris.fr/planning-instance',
        [],
        ['context' => 'warcraft_core']
      );
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
  }

}
