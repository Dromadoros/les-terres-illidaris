<?php

namespace Drupal\warcraft_core\Command;

use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class ResetVotesPoints
 *
 * @package Drupal\warcraft_core\Command
 */
class ResetVotesPoints extends DrushCommands {

  /**
   * Reset votes
   *
   * @command user:reset-votes
   * @aliases u:sv
   * @usage user:reset-votes
   *   Reset votes
   */
  public function resetVotes() {
    $users = \Drupal::entityQuery('user')->execute();
    $users = User::loadMultiple($users);

    /** @var User $user */
    foreach ($users as $user) {
      $user->set('field_user_votes', 0);
      $user->save();
    }
  }

}
