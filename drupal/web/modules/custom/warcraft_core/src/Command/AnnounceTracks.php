<?php

namespace Drupal\warcraft_core\Command;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * Class AnnounceTracks
 *
 * @package Drupal\warcraft_core\Command
 */
class AnnounceTracks extends DrushCommands {

  /**
   * Set default votes points to user by their monnaie
   *
   *
   * @command track:announce
   * @aliases t:a
   * @usage track:announce
   *   Announce all active tracks
   */
  public function announceTracks() {
    $results = \Drupal::entityQuery('node')
      ->condition('type', 'track')
      ->condition('status', Node::PUBLISHED)
      ->condition('field_track_has_a_winner', 0)
      ->execute();

    if (!$results) {
      return;
    }

    $nodes = Node::loadMultiple($results);

    $command = ".announce " . t(
        'Les Tracks actives : ',
        [],
        ['context' => 'warcraft_core']
      );
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
    $command = ".announce -------------------------------------------------";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    foreach ($nodes as $node) {
      /** @var \Drupal\node\NodeInterface $node */
      $title = t(
        'Track : @title',
        ['@title' => $node->getTitle()],
        ['context' => 'ucm_core']
      );

      $command = ".announce " . htmlspecialchars_decode($title);
      shell_exec(
        "sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'"
      );
    }

    $command = ".announce -------------------------------------------------";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".announce Accomplissez une track et gagnez de grosses récompenses.";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".announce Indices des tracks sur : https://les-terres-illidaris.fr/les-tracks";
    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
  }

  /**
   * @param \Drupal\Core\Database\Connection $db
   * @param $entry
   */
  private function setSpawntime(Connection $db, $entry) {
    $db->update('creature')
      ->condition('id', $entry)
      ->fields(
        [
          'spawntimesecsmin' => 600000000,
          'spawntimesecsmax' => 600000000,
        ]
      )->execute();
  }

}
