<?php

namespace Drupal\warcraft_core\Constants;

/**
 * Class Paypal
 *
 * @package Drupal\warcraft_core\Constants
 */
final class Paypal {

    const AMOUNTS = [5, 10, 20];
}
