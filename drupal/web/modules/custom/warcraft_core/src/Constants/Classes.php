<?php

namespace Drupal\warcraft_core\Constants;

/**
 * Class Classes
 *
 * @package Drupal\warcraft_core\Constants
 */
final class Classes {

  const WAR = '1';
  const PALADIN = '2';
  const HUNTER = '3';
  const ROGUE = '4';
  const PRIEST = '5';
  const SHAMAN = '7';
  const MAGE = '8';
  const WARLOCK = '9';
  const DRUID = '11';
}
