<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\warcraft_core\Form\ContactForm;

/**
 * Provides a 'InstanceListBlock' Block.
 *
 * @Block(
 *   id = "instance_list_block",
 *   admin_label = @Translation("Instance List block"),
 *   category = @Translation("Mangos"),
 * )
 */
class InstanceListBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $now = new DrupalDateTime('now');

    $instances = \Drupal::entityQuery('node')
      ->condition('type', 'instance_pool')
      ->condition('status', Node::PUBLISHED)
      ->condition(
        'field_instance_date',
        $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '>='
      )
      ->sort('field_instance_date', 'ASC')
      ->execute();

    $pastInstances = \Drupal::entityQuery('node')
      ->condition('type', 'instance_pool')
      ->condition('status', Node::PUBLISHED)
      ->condition(
        'field_instance_date',
        $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        '<'
      )
      ->sort('field_instance_date', 'DESC')
      ->range(0, 9)
      ->execute();

    $instances = Node::loadMultiple($instances);
    $pastInstances = Node::loadMultiple($pastInstances);

    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $instances = $viewBuilder->viewMultiple($instances, 'teaser');
    $pastInstances = $viewBuilder->viewMultiple($pastInstances, 'teaser');

    return [
      '#theme' => 'instance_list',
      '#instances' => $instances,
      '#pastInstances' => $pastInstances,
    ];
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
