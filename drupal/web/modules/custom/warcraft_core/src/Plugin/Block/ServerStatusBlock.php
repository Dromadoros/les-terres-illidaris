<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\warcraft_core\Constants\Races;

/**
 * Provides a 'Subscription' Block.
 *
 * @Block(
 *   id = "server_status_block",
 *   admin_label = @Translation("Server status block"),
 *   category = @Translation("Mangos"),
 * )
 */
class ServerStatusBlock extends BlockBase implements BlockPluginInterface {

  /**
   * @inheritDoc
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $server = Settings::get('server_url');
    $isServerUp = FALSE !== fsockopen($server, 3724) && FALSE !== fsockopen($server, 8085);
    Database::setActiveConnection('realmd');
    try {
      $db = Database::getConnection();
      $uptimes = $db->select('uptime', 'u')->fields('u', ['starttime', 'maxplayers'])->orderBy('starttime', 'DESC')->execute()->fetchAll();
    } catch (\PDOException $e) {
      \Drupal::logger('warcraft_core')->error($e->getMessage());
    }

    Database::setActiveConnection('character');
    try {
      $db = Database::getConnection();
      $characters = $db->select('characters', 'c')->fields('c', ['race'])->execute()->fetchAll();
      $allianceCharacters = count(array_filter($characters, function($character) {
        return in_array($character->race, Races::ALLIANCE_RACES_IDS);
      }));
      $totalCharacters = count($characters);
    } catch (\PDOException $e) {
      \Drupal::logger('warcraft_core')->error($e->getMessage());
    }


    Database::setActiveConnection();

    $nid = \Drupal::config('warcraft_core.settings')->get('realm');

    return [
      '#theme' => 'server_status',
      '#isServerUp' => $isServerUp,
      '#characters' => [
        'horde' => (($totalCharacters - $allianceCharacters) / $totalCharacters) * 100,
        'alliance' => ($allianceCharacters / $totalCharacters) * 100,
      ],
      '#uptime' => $uptimes ? reset($uptimes)->starttime : time(),
      '#realm' => $nid ? Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString() : NULL,
    ];
  }

}
