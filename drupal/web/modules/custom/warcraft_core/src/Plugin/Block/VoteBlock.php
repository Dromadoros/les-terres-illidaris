<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\warcraft_core\Form\VoteForm;

/**
 * Provides a 'Vote' Block.
 *
 * @Block(
 *   id = "vote_block",
 *   admin_label = @Translation("Vote block"),
 *   category = @Translation("Mangos"),
 * )
 */
class VoteBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm(VoteForm::class);

    return $form;
  }

}
