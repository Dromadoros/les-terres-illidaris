<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\warcraft_core\Form\HardcoreRecuperationForm;

/**
 * Provides a 'Hardcore Recuperation' Block.
 *
 * @Block(
 *   id = "hardcore_recuperation_block",
 *   admin_label = @Translation("Hardcore Recuperation block"),
 *   category = @Translation("Mangos"),
 * )
 */
class HardcoreRecuperationBlock extends BlockBase implements BlockPluginInterface {

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm(HardcoreRecuperationForm::class);

    return $form;
  }

}
