<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\warcraft_core\Form\ContactForm;
use Drupal\warcraft_core\Form\DonationForm;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a 'Donation block' Block.
 *
 * @Block(
 *   id = "donation_block",
 *   admin_label = @Translation("Donation block"),
 *   category = @Translation("Mangos"),
 * )
 */
class DonationBlock extends BlockBase implements BlockPluginInterface {

    /**
     * {@inheritdoc}
     */
    public function build() {
      $form = \Drupal::formBuilder()->getForm(DonationForm::class);

      return $form;
    }

}
