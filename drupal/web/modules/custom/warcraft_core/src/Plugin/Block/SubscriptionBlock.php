<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\warcraft_core\Form\SubscriptionForm;

/**
 * Provides a 'Subscription' Block.
 *
 * @Block(
 *   id = "subscription_block",
 *   admin_label = @Translation("Subscription block"),
 *   category = @Translation("Mangos"),
 * )
 */
class SubscriptionBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm(SubscriptionForm::class);

    return $form;
  }

}
