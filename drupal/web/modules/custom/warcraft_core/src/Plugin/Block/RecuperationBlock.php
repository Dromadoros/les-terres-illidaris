<?php

namespace Drupal\warcraft_core\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\warcraft_core\Form\RecuperationForm;

/**
 * Provides a 'Recuperation' Block.
 *
 * @Block(
 *   id = "recuperation_block",
 *   admin_label = @Translation("Recuperation block"),
 *   category = @Translation("Mangos"),
 * )
 */
class RecuperationBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm(RecuperationForm::class);

    return $form;
  }

}
