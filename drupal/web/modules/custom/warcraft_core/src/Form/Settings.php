<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class SettingsForm
 *
 * @package Drupal\warcraft_core\Form
 */
class Settings extends ConfigFormBase {

  /**
   * @return array|string
   */
  protected function getEditableConfigNames() {
    return ['warcraft_core.settings'];
  }

  /**
   * @return string
   */
  public function getFormId() {
    return 'warcraft_settings';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('warcraft_core.settings');
    $home = $config->get('homepage');
    $news = $config->get('news');
    $highlighted = $config->get('highlighted');
    $hyjal = $config->get('hyjal');

    $form['warcraft_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t(
        'Warcraft - settings',
        [],
        ['context' => 'warcraft_core']
      ),
      '#tree' => FALSE,
      '#open' => TRUE,
    ];

    $form['warcraft_wrapper']['homepage'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Homepage', [], ['context' => 'warcraft_core']),
      '#default_value' => $home ? Node::load($home) : NULL,
      '#selection_settings' => [
        'target_bundles' => ['system_page'],
      ],
    ];

    $form['warcraft_wrapper']['news'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('News', [], ['context' => 'warcraft_core']),
      '#default_value' => $news ? Node::load($news) : NULL,
      '#selection_settings' => [
        'target_bundles' => ['system_page'],
      ],
    ];

    $form['warcraft_wrapper']['hyjal'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Hyjal', [], ['context' => 'warcraft_core']),
      '#default_value' => $hyjal ? Node::load($hyjal) : NULL,
    ];

    $form['warcraft_wrapper']['header_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Header video', [], ['context' => 'warcraft_core']),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t(
          'This video will be put directly in the header',
          [],
          ['context' => 'warcraft_core']
        ),
      ],
      '#size' => 50,
      '#default_value' => $config->get('header_file'),
      '#upload_location' => 'public://header/',
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'file_validate_extensions' => ['mp4 ogg mpg'],
      ],
    ];

    $form['warcraft_wrapper_mangos'] = [
      '#type' => 'details',
      '#title' => $this->t(
        'Warcraft - Mangos',
        [],
        ['context' => 'warcraft_core']
      ),
      '#tree' => FALSE,
      '#open' => TRUE,
    ];

    $form['warcraft_wrapper_mangos']['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Launcher version', [], ['context' => 'warcraft_core']),
      '#default_value' => $config->get('version') ?: '',
    ];

    $form['warcraft_wrapper_mangos']['special'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Special', [], ['context' => 'warcraft_core']),
      '#default_value' => $config->get('special') ?: '',
    ];

    $form['warcraft_wrapper_mangos']['highlighted'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Highlighted', [], ['context' => 'warcraft_core']),
      '#default_value' => $home ? Node::load($highlighted) : NULL,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save', [], ['context' => 'warcraft_core']),
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('warcraft_core.settings');
    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();
  }

}
