<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Laizerox\Wowemu\SRP\UserClient;

/**
 * Class SubscriptionForm.
 */
class SubscriptionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
      '#description' => $this->t('A l\'inscription, recevez <span>49 marques Illidaris</span> ! A 50 marques vous aurez droit à un <span><b>sésame</b></span>, il faudra seulement faire 1 vote !', [], ['context' => 'warcraft_core']),
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password', [], ['context' => 'warcraft_core']),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password_confirm',
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
    ];
    $form['parrain'] = [
      '#type' => 'email',
      '#title' => $this->t(
        'Parrain e-mail (optionnel)',
        [],
        ['context' => 'warcraft_core']
      ),
      '#description' => $this->t(
        'Lorsque votre parrain et vous atteignez le niveau 70 : <span>vous recevrez 150 marques Illidaris</span> utilisables en boutique !',
        [],
        ['context' => 'warcraft_core']
      ),
      '#required' => FALSE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t(
        'Créer le compte',
        [],
        ['context' => 'warcraft_core']
      ),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    Database::setActiveConnection('realmd');
    $values = $form_state->getValues();
    $username = $values['username'];
    $password = $values['password'];
    $parrain = $values['parrain'];

    $existingUser = \Drupal::entityQuery('user')
      ->condition('mail', $parrain)
      ->execute();

    if ($parrain && empty($existingUser)) {
      \Drupal::messenger()->addError(
        $this->t(
          'L\'adresse mail du parrain est inexistante.',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return;
    }

    if (strlen($username) > 16 || strlen($password) > 16) {
      \Drupal::messenger()->addError(
        $this->t(
          'Votre compte utilisateur/mot de passe ne doit pas dépasser 16 caractères',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return;
    }

    $existingAccounts = \Drupal::entityQuery('user')
      ->condition('mail', $values['email'])
      ->execute();

    if (!empty($existingAccounts)) {
      \Drupal::messenger()->addError(
        $this->t(
          'Un compte est déjà existant avec cet e-mail.',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return;
    }

    $db = Database::getConnection();

    $existingUsers = $db->query(
      'SELECT * FROM account WHERE account.username= \'' . $username . '\''
    )->fetchAll();

    if ($existingUsers) {
      \Drupal::messenger()->addError(
        $this->t(
          'Un compte est déjà existant avec ce compte utilisateur',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return;
    }
    /* Create your v and s values. */
    $client = new UserClient($username);
    $salt = $client->generateSalt();
    $verifier = $client->generateVerifier($password);

    $db->insert('account')->fields(
      [
        'username' => $username,
        'email' => $values['email'],
        'v' => $verifier,
        's' => $salt,
        'expansion' => 1,
      ]
    )->execute();

    Database::setActiveConnection();

    /** @var \Drupal\user\Entity\User $user */
    $user = \Drupal\user\Entity\User::create(
      [
        'name' => $username,
        'pass' => $password,
        'mail' => $values['email'],
        'status' => 1,
        'init' => $values['email'],
        'field_user_points' => 49,
        'field_user_parrain_email' => $parrain ?: NULL,
        'field_user_has_parrain_gift' => FALSE,
      ]
    );

    $user->save();

    $to = \Drupal::config('system.site')->get('mail');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');
    $mailManager->mail(
      'warcraft_core',
      'subscription',
      $values['email'],
      $langcode,
      $values
    );
    $mailManager->mail(
      'warcraft_core',
      'subscription',
      $to,
      $langcode,
      $values
    );

    \Drupal::messenger()->addMessage(
      $this->t('Compte créé avec succès.', [], ['context' => 'warcraft_core'])
    );
  }

}
