<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class VoteForm.
 */
class VoteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vote_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $username = \Drupal::currentUser()->isAnonymous() ? '' : \Drupal::currentUser()->getAccountName();

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
      '#default_value' => $username,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Retrieve point', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \ExceptionS
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $username = $values['user'];
    $API_key = \Drupal\Core\Site\Settings::get('vote_token');
    $API_ip = \Drupal::request()->getClientIp();
    $json = file_get_contents("https://serveur-prive.net/api/vote/json/$API_key/$API_ip");
    $json_data = json_decode($json, TRUE);

    $user = \Drupal::entityQuery('user')
      ->condition('name', $username)
      ->execute();

    if (empty($user) || (int) reset($user) === 0) {
      \Drupal::messenger()->addMessage(
        $this->t('Your account cannot be found', [], ['context' => 'warcraft_core'])
      );

      return;
    }

    /** @var \Drupal\user\Entity\User $user */
    $user = User::load(reset($user));
    $lastVoted = $user->get('field_user_last_voted')->value ?: 0;

    if ((int) $json_data['status'] !== 1 || ((time() - $lastVoted) < 5400) ) {
      \Drupal::messenger()->addError(
        $this->t('You cannot vote yet, try again later ...', [], ['context' => 'warcraft_core'])
      );

      return;
    }

    $points = $user->get('field_user_points')->value ?: 0;
    $votes = $user->get('field_user_votes')->value ?: 0;
    $points += 1;
    $votes +=1;

    $user->set('field_user_points', $points);
    $user->set('field_user_votes', $votes);
    $user->set('field_user_last_voted', time());
    $user->save();

    \Drupal::messenger()->addMessage(
      $this->t(
        'Vote registred, thank you. One point is added to your account. You have now @points points',
        ['@points' => $points],
        ['context' => 'warcraft_core']
      )
    );
  }

}
