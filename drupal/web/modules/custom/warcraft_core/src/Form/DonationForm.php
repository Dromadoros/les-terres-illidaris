<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class DonationForm.
 */
class DonationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'donation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['amount'] = [
      '#type' => 'select',
      '#title' => $this->t('Amount', [], ['context' => 'warcraft_core']),
      '#default_value' => '5',
      '#options' => [
        '5' => $this->t('5€ (50 marques)', [], ['context' => 'ucm_core']),
        '10' => $this->t('10€ (115 marques)', [], ['context' => 'ucm_core']),
        '20' => $this->t('20€ (250 marques)', [], ['context' => 'ucm_core']),
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $amount = $values['amount'];

    $url = Url::fromRoute('warcraft_core.paypal_pay', ['amount' => $amount]);
    $form_state->setRedirectUrl($url);
  }

}
