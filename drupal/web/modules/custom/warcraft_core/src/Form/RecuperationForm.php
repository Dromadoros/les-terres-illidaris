<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\warcraft_shop\Constants\Shop;

/**
 * Class RecuperationForm.
 */
class RecuperationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recuperation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $roles = \Drupal::currentUser()->getRoles();
    $characters = $characterManager->getUserCharacters(1, 10);
    $maxLevelCharacters = $characterManager->getUserCharacters(70);
    $hasMaxLevel = !empty($maxLevelCharacters) && !in_array('moderator', $roles);

    if ($hasMaxLevel) {
      $form['already_max_level'] = [
        '#type' => 'label',
        '#attributes' => [
          'class' => [
            'already-max-level-label',
          ],
        ],
        '#title' => $this->t(
          'Vous êtes déjà en possession d\'au moins un personnage niveau 70',
          [],
          ['context' => 'warcraft_core']
        ),
        '#title_display' => 'above',
      ];
    }

    $formattedCharacters = [];

    foreach ($characters as $character) {
      $formattedCharacters[$character['guid'] . '_' . $character['name']] = $character['name'];
    }

    $classesSpe = [];
    $currentGroup = 'none';

    foreach (Shop::RECUP_ITEMSET as $key => $itemset) {
      if (!array_key_exists('weapon', $itemset)) {
        $currentGroup = $itemset['label'];
        continue;
      }

      $classesSpe[$currentGroup][$key] = $itemset['label'];
    }

    $professions = [
      NULL => $this->t('Pas de métier', [], ['context' => 'warcraft_core']),
      'alchimie' => $this->t('Alchimiste', [], ['context' => 'warcraft_core']),
      'forge' => $this->t('Forgeron', [], ['context' => 'warcraft_core']),
      'enchanteur' => $this->t(
        'Enchanteur',
        [],
        ['context' => 'warcraft_core']
      ),
      'ingenieur' => $this->t('Ingénieur', [], ['context' => 'warcraft_core']),
      'herboriste' => $this->t(
        'Herboriste',
        [],
        ['context' => 'warcraft_core']
      ),
      'tdc' => $this->t('Travail de cuir', [], ['context' => 'warcraft_core']),
      'joa' => $this->t('Joaillerie', [], ['context' => 'warcraft_core']),
      'mineur' => $this->t('Mineur', [], ['context' => 'warcraft_core']),
      'depeceur' => $this->t('Dépeceur', [], ['context' => 'warcraft_core']),
      'tailleur' => $this->t('Tailleur', [], ['context' => 'warcraft_core']),
    ];

    $form['player'] = [
      '#type' => 'select',
      '#title' => $this->t('Personnage', [], ['context' => 'warcraft_core']),
      '#description' => $this->t(
        'Le nom de votre personnage sur lequel le transfert doit se faire',
        [],
        ['context' => 'warcraft_core']
      ),
      '#options' => $formattedCharacters,
      '#required' => TRUE,
    ];
    $form['server_web'] = [
      '#type' => 'textfield',
      '#title' => $this->t(
        'Site du serveur auquel votre personnage appartient',
        [],
        ['context' => 'warcraft_core']
      ),
      '#required' => TRUE,
    ];
    $form['class'] = [
      '#type' => 'select',
      '#title' => $this->t('Classe - spé', [], ['context' => 'warcraft_core']),
      '#default_value' => '5',
      '#options' => $classesSpe,
    ];
    $form['profession1'] = [
      '#type' => 'select',
      '#title' => $this->t('Métier 1', [], ['context' => 'warcraft_core']),
      '#description' => $this->t(
        'Remplissez ce champs seulement si votre métier est au maximum',
        [],
        ['context' => 'warcraft_core']
      ),
      '#default_value' => '5',
      '#options' => $professions,
    ];
    $form['profession2'] = [
      '#type' => 'select',
      '#title' => $this->t('Métier 2', [], ['context' => 'warcraft_core']),
      '#description' => $this->t(
        'Remplissez ce champs seulement si votre métier est au maximum',
        [],
        ['context' => 'warcraft_core']
      ),
      '#default_value' => '5',
      '#options' => $professions,
    ];
    $form['file_label'] = [
      '#type' => 'label',
      '#attributes' => [
        'class' => [
          'files-label',
        ],
      ],
      '#title' => $this->t(
        'Importation des preuves photos',
        [],
        ['context' => 'warcraft_core']
      ),
      '#title_display' => 'above',
    ];
    $form['list_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t(
        'Photo de votre personnage sur l\'écran liste de personnages',
        [],
        ['context' => 'warcraft_track']
      ),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t(
          'Maximum size: 5mb',
          [],
          ['context' => 'warcraft_core']
        ),
      ],
      '#required' => TRUE,
      '#size' => 5,
      '#upload_location' => 'public://recuperation/proof_list/',
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
      ],
    ];
    $form['professions_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t(
        'Photo de vos compétences métiers (screenshot écran entier)',
        [],
        ['context' => 'warcraft_track']
      ),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t(
          'Maximum size: 5mb',
          [],
          ['context' => 'warcraft_core']
        ),
      ],
      '#size' => 5,
      '#upload_location' => 'public://recuperation/proof_professions/',
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
      ],
    ];
    $form['played_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t(
        'Photo de votre /played en jeu (screenshot écran entier)',
        [],
        ['context' => 'warcraft_track']
      ),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t(
          'Maximum size: 5mb',
          [],
          ['context' => 'warcraft_core']
        ),
      ],
      '#required' => TRUE,
      '#size' => 5,
      '#upload_location' => 'public://recuperation/proof_played/',
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
      ],
    ];
    $form['agree_burning_crusade'] = [
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Confirmez-vous que votre personnage provient d\'un serveur Burning Crusade ?',
        [],
        ['context' => 'warcraft_core']
      ),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    if ($hasMaxLevel) {
      $form['list_file']['#attributes']['disabled'] = 'disabled';
      $form['professions_file']['#attributes']['disabled'] = 'disabled';
      $form['played_file']['#attributes']['disabled'] = 'disabled';
      $form['submit']['#attributes']['disabled'] = 'disabled';
    }

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $character = $values['player'];
    $fileList = $values['list_file'];
    $filePro = $values['professions_file'];
    $filePlayed = $values['played_file'];
    $mediaPro = NULL;

    $mediaList = Media::create(
      [
        'bundle' => 'image',
        'title' => 'Récupération - List ' . $character,
        'field_media_image' => [
          'target_id' => reset($fileList),
        ],
      ]
    );

    if ($filePro) {
      $mediaPro = Media::create(
        [
          'bundle' => 'image',
          'title' => 'Récupération - Profession ' . $character,
          'field_media_image' => [
            'target_id' => reset($filePro),
          ],
        ]
      );

      $mediaPro->save();
    }

    $mediaPlayed = Media::create(
      [
        'bundle' => 'image',
        'title' => 'Récupération - Played ' . $character,
        'field_media_image' => [
          'target_id' => reset($filePlayed),
        ],
      ]
    );

    $mediaList->save();
    $mediaPlayed->save();

    $professions = [];
    $profession1 = $values['profession1'];
    $profession2 = $values['profession2'];

    if ($profession1) {
      $professions[] = $profession1;
    }

    if ($profession2) {
      $professions[] = $profession2;
    }

    Node::create(
      [
        'type' => 'demande_recuperation',
        'status' => 0,
        'title' => 'Demande récupération - ' . $character,
        'field_recup_name' => $character,
        'field_recup_profession' => $professions,
        'field_recup_website' => $values['server_web'],
        'field_recup_spe' => $values['class'],
        'field_recup_is_open' => 1,
        'field_recup_proof_list' => [
          'target_id' => $mediaList->id(),
        ],
        'field_recup_proof_professions' => [
          'target_id' => $mediaPro ? $mediaPro->id() : NULL,
        ],
        'field_recup_proof_played' => [
          'target_id' => $mediaPlayed->id(),
        ],
      ]
    )->save();

    $userId = \Drupal::currentUser()->id();
    $user = User::load($userId);
    $user->set('field_user_has_recuperation', 1);
    $user->save();

    \Drupal::messenger()->addMessage(
      $this->t(
        'Votre demande a bien été enregistrée, vous recevrez une réponse dans les plus brefs délais',
        [],
        ['context' => 'warcraft_core']
      )
    );
  }

}
