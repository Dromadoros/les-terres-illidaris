<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Class InstanceBookForm.
 */
class InstanceBookForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instance_book_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = \Drupal::routeMatch()->getParameter('node');

    if (!$node || 'instance_pool' !== $node->getType()) {
      return [];
    }

    /** @var \Drupal\node\NodeInterface $instance */
    $instance = $node->get('field_instance_reference')->entity;
    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $characters = $characterManager->getUserCharacters($instance->get('field_instance_required_level')->value);
    $charactersSelect = [];

    foreach ($characters as $character) {
      $charactersSelect[$character['name']] = $character['name'];
    }

    $form['player'] = [
      '#type' => 'select',
      '#title' => $this->t('Personnage', [], ['context' => 'warcraft_core']),
      '#default_value' => '5',
      '#options' => $charactersSelect,
      '#required' => TRUE,
    ];
    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role (ou autre)', [], ['context' => 'warcraft_core']),
      '#default_value' => 'tank',
      '#options' => [
        'Role' => [
          'tank' => t('Tank', [], ['context' => 'warcraft_core']),
          'heal' => t('Heal', [], ['context' => 'warcraft_core']),
          'dps' => t('Dps', [], ['context' => 'warcraft_core']),
        ],
        'Autres' => [
          'maybe' => t('Indécis(e)', [], ['context' => 'warcraft_core']),
          'not' => t('Indisponible', [], ['context' => 'warcraft_core']),
        ],
      ],
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = \Drupal::routeMatch()->getParameter('node');
    $values = $form_state->getValues();
    $name = $values['player'];

    $participants = $node->get('field_instance_participants')
      ->referencedEntities();
    $participants = array_filter(
      $participants,
      function (Paragraph $participant) use ($name) {
        return $participant->get('field_participant_player')->value !== $name;
      }
    );

    $participantsData = array_map(
      function (Paragraph $participant) {
        return [
          'target_id' => $participant->id(),
          'target_revision_id' => $participant->getRevisionId(),
        ];
      },
      $participants
    );

    $paragraph = Paragraph::create(
      [
        'type' => 'instance_participant',
        'field_participant_player' => $values['player'],
        'field_participant_role' => $values['role'],
      ]
    );

    $paragraph->save();

    $paragraphData = [
      [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ],
    ];

    $node->set(
      'field_instance_participants',
      array_merge($participantsData, $paragraphData)
    );
    $node->save();

    \Drupal::messenger()->addMessage(
      $this->t(
        'Vous vous êtes bien enregistré à l\'instance',
        [],
        ['context' => 'warcraft_core']
      )
    );
  }

}
