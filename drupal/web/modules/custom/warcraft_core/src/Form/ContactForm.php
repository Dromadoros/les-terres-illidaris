<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactForm.
 */
class ContactForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
    ];
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text', [], ['context' => 'warcraft_core']),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $to = \Drupal::config('system.site')->get('mail');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');
    $result = $mailManager->mail('warcraft_core', 'contact', $to, $langcode, $values);

    if (!$result) {
      \Drupal::messenger()->addError($this->t('Message not sent correctly', [], ['context' => 'warcraft_core']));
      return;
    }

    \Drupal::messenger()->addMessage($this->t('Message sent with success', [], ['context' => 'warcraft_core']));
  }

}
