<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Paypal payment entities.
 *
 * @ingroup warcraft_core
 */
class PaypalPaymentDeleteForm extends ContentEntityDeleteForm {


}
