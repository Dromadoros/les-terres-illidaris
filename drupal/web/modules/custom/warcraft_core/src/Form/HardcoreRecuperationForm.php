<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\warcraft_core\Constants\Races;
use Drupal\warcraft_shop\Constants\Shop;

/**
 * Class HardcoreRecuperationForm.
 */
class HardcoreRecuperationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hardcore_recuperation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $userId = \Drupal::currentUser()->id();
    $user = User::load($userId);
    $hasReceivedGift = $user->get('field_user_hardcore_gift')->value;

    if ($hasReceivedGift) {
      $form['already_max_level'] = [
        '#type' => 'label',
        '#attributes' => [
          'class' => [
            'already-max-level-label',
          ],
        ],
        '#title' => $this->t(
          'Vous avez déjà reçu votre cadeau du mode Hardcore.',
          [],
          ['context' => 'warcraft_core']
        ),
        '#title_display' => 'above',
      ];

      return $form;
    }

    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $charactersHardcore = $characterManager->getUserCharacters(
      70,
      70,
      'survival_character'
    );

    if (empty($charactersHardcore)) {
      $form['already_max_level'] = [
        '#type' => 'label',
        '#attributes' => [
          'class' => [
            'already-max-level-label',
          ],
        ],
        '#title' => $this->t(
          'Vous n\'avez pas encore de niveau 70 sur le mode hardcore.',
          [],
          ['context' => 'warcraft_core']
        ),
        '#title_display' => 'above',
      ];

      return $form;
    }

    $characters = $characterManager->getUserCharacters(1, 10);

    $formattedCharacters = [];

    foreach ($characters as $character) {
      $formattedCharacters[$character['guid']] = $character['name'];
    }

    $classesSpe = [];
    $currentGroup = 'none';

    foreach (Shop::RECUP_ITEMSET as $key => $itemset) {
      if (!array_key_exists('weapon', $itemset)) {
        $currentGroup = $itemset['label'];
        continue;
      }

      $classesSpe[$currentGroup][$key] = $itemset['label'];
    }

    $form['player'] = [
      '#type' => 'select',
      '#title' => $this->t('Personnage', [], ['context' => 'warcraft_core']),
      '#description' => $this->t(
        'Le nom de votre personnage sur lequel le transfert doit se faire (vous devez créer un personnage niveau 1 sur le royaume principal)',
        [],
        ['context' => 'warcraft_core']
      ),
      '#options' => $formattedCharacters,
      '#required' => TRUE,
    ];
    $form['class'] = [
      '#type' => 'select',
      '#title' => $this->t('Classe - spé', [], ['context' => 'warcraft_core']),
      '#default_value' => '5',
      '#options' => $classesSpe,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $characterId = $values['player'];
    $spe = $values['class'];
    /** @var \Drupal\warcraft_user\Services\CharacterManager $characterManager */
    $characterManager = \Drupal::service('warcraft_user.character_manager');
    $character = $characterManager->getCharacterData($characterId);

    Database::setActiveConnection('character');
    $db = Database::getConnection();

    if (!(bool) $character['totaltime']) {
      \Drupal::messenger()->addError(
        t(
          'You need to log-in once in game',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }
    if ((bool) $character['online']) {
      \Drupal::messenger()->addError(
        t(
          'Le personnage doit être deconnecté',
          [],
          ['context' => 'warcraft_core']
        )
      );

      return FALSE;
    }

    /** @var \Drupal\warcraft_shop\Service\ShopManager $shopManager */
    $shopManager = \Drupal::service('warcraft_shop.shop_manager');

    $isAlliance = in_array($character['race'], Races::ALLIANCE_RACES_IDS);

    $mainCharactersData = [
      'level' => 70,
      'xp' => 0,
      'money' => 10000000,
      'taximask' => $isAlliance ?
        '3456411898 2148078929 805356359 2605711384 137366529 262240 1052676 0' :
        '830150144 315656864 449720 3869245476 3227522050 262180 1048576 0',
    ];
    $query = $db->update('character_reputation');

    $query
      ->condition($shopManager->getFactionsConditionGroup($character, $query))
      ->condition('guid', $character['guid'])
      ->fields(
        [
          'standing' => 20000,
          'flags' => 17,
        ]
      )
      ->execute();

    $db->update('characters')->condition('guid', $character['guid'])->fields(
      array_merge(
        $mainCharactersData,
        $shopManager->getSesamePosition($character)
      )
    )->execute();

    $shopManager->learnSpell($db, $characterId, 34091);

    $characterName = $character['name'];

    $shopManager->updateSesameSkills($db, $character, 350);
    $itemset = Shop::RECUP_ITEMSET[$spe];
    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Itemset',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['set'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Bagues et bijoux',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['trinkets'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Armes',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $itemset['weapon'];

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Sacs',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . Shop::BAG_12;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");

    $mount = $isAlliance ? Shop::MOUNT_ALLIANCE : Shop::MOUNT_HORDE;

    $command = ".send items " . $characterName . " \"" . t(
        '[Récupération] Monture',
        [],
        ['warcraft_shop']
      ) . "\" \"\" " . $mount;

    shell_exec("sudo screen -S root/mangosd_normal -p 0 -X stuff '$command^M'");
    $shopManager->learnClassSpells($db, $character);
    $userId = \Drupal::currentUser()->id();
    $user = User::load($userId);
    $user->set('field_user_hardcore_gift', 1);
    $user->save();

    \Drupal::messenger()->addMessage(
      $this->t(
        'Votre personnage a bien été augmenté niveau 70 avec le stuff adéquat. Bon jeu à vous !',
        [],
        ['context' => 'warcraft_core']
      )
    );
  }

}
