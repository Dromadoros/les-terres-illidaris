<?php

namespace Drupal\warcraft_core\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ValidateRecuperationForm.
 */
class ValidateRecuperationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'validate_recuperation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = \Drupal::routeMatch()->getParameter('node');
    $isOpen = $node->get('field_recup_is_open')->value;
    $userId = \Drupal::currentUser()->id();
    $user = User::load($userId);

    if ((int) $user->id() !== 1 && !$user->hasRole('moderator')) {
      return [];
    }

    if (!$isOpen) {
      return [];
    }

    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t(
        'Décision de la demande',
        [],
        ['context' => 'warcraft_core']
      ),
      '#options' => [
        'accept' => $this->t('Accepter', [], ['context' => 'warcraft_core']),
        'refuse' => $this->t('Refuser', [], ['context' => 'warcraft_core']),
      ],
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t(
        'Ce message sera envoyé par mail à l\'utilisateur, seulement si la demande a été refusé.',
        [],
        ['context' => 'warcraft_core']
      ),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send', [], ['context' => 'warcraft_core']),
    ];

    $form['#theme'] = 'warcraft_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = \Drupal::routeMatch()->getParameter('node');
    $to = $node->getOwner()->getEmail();
    $values = $form_state->getValues();
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    if ($values['status'] === 'accept') {
      $title = $this->t(
        'Votre demande de récupération a été aceptée !',
        [],
        ['context' => 'ucm_core']
      );
      $message = $this->t(
        'Vous pouvez désormais vous connecter en jeu afin de profiter de votre personnage à jour. Bon jeu à vous !',
        [],
        ['context' => 'ucm_core']
      );

      $isPlayerUp = $this->doRecup(
        $node->get('field_recup_name')->value,
        $node->get('field_recup_profession')->getValue(),
        $node->get('field_recup_spe')->value
      );

      if (!$isPlayerUp) {
        \Drupal::messenger()->addError(
          $this->t(
            'Le up du player a eu une erreur',
            [],
            ['context' => 'warcraft_core']
          )
        );
        return;
      }
    }
    else {
      $title = $this->t(
        'Votre demande de récupération a été refusée.',
        [],
        ['context' => 'ucm_core']
      );
      $message = $values['message'];

      //Init the flag so he can ask again
      $userId = \Drupal::currentUser()->id();
      $user = User::load($userId);
      $user->set('field_user_has_recuperation', 0);
      $user->save();
    }

    $node->set('field_recup_is_open', 0);
    $node->save();

    $values['title'] = $title;
    $values['message'] = $message;

    /** @var \Drupal\Core\Mail\MailManager $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');
    $result = $mailManager->mail(
      'warcraft_core',
      'recuperation',
      $to,
      $langcode,
      $values
    );

    if (!$result) {
      \Drupal::messenger()->addError(
        $this->t(
          'Message not sent correctly',
          [],
          ['context' => 'warcraft_core']
        )
      );
      return;
    }

    \Drupal::messenger()->addMessage(
      $this->t(
        'Le status de la demande a été cloturé et la demande envoyée',
        [],
        ['context' => 'warcraft_core']
      )
    );
  }

  /**
   * @param $characterName
   * @param $professions
   * @param $spe
   *
   * @return bool
   * @throws \Exception
   */
  private function doRecup($characterName, $professions, $spe) {
    $characterId = explode('_', $characterName)[0];

    /** @var \Drupal\warcraft_shop\Service\ShopManager $shopManager */
    $shopManager = \Drupal::service('warcraft_shop.shop_manager');
    $shopManager->doBoost($characterId, $professions, $spe);
  }


}
