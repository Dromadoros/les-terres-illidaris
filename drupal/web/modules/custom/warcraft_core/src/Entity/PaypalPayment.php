<?php

namespace Drupal\warcraft_core\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Paypal payment entity.
 *
 * @ingroup warcraft_core
 *
 * @ContentEntityType(
 *   id = "paypal_payment",
 *   label = @Translation("Paypal payment"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\warcraft_core\PaypalPaymentListBuilder",
 *     "views_data" = "Drupal\warcraft_core\Entity\PaypalPaymentViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\warcraft_core\Form\PaypalPaymentForm",
 *       "add" = "Drupal\warcraft_core\Form\PaypalPaymentForm",
 *       "edit" = "Drupal\warcraft_core\Form\PaypalPaymentForm",
 *       "delete" = "Drupal\warcraft_core\Form\PaypalPaymentDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\warcraft_core\PaypalPaymentHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\warcraft_core\PaypalPaymentAccessControlHandler",
 *   },
 *   base_table = "paypal_payment",
 *   translatable = FALSE,
 *   admin_permission = "administer paypal payment entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/paypal_payment/{paypal_payment}",
 *     "add-form" = "/admin/structure/paypal_payment/add",
 *     "edit-form" = "/admin/structure/paypal_payment/{paypal_payment}/edit",
 *     "delete-form" = "/admin/structure/paypal_payment/{paypal_payment}/delete",
 *     "collection" = "/admin/structure/paypal_payment",
 *   },
 *   field_ui_base_route = "paypal_payment.settings"
 * )
 */
class PaypalPayment extends ContentEntityBase implements PaypalPaymentInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Paypal payment entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Paypal payment is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
