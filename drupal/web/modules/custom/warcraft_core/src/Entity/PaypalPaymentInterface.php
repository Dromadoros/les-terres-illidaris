<?php

namespace Drupal\warcraft_core\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Paypal payment entities.
 *
 * @ingroup warcraft_core
 */
interface PaypalPaymentInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Paypal payment name.
   *
   * @return string
   *   Name of the Paypal payment.
   */
  public function getName();

  /**
   * Sets the Paypal payment name.
   *
   * @param string $name
   *   The Paypal payment name.
   *
   * @return \Drupal\warcraft_core\Entity\PaypalPaymentInterface
   *   The called Paypal payment entity.
   */
  public function setName($name);

  /**
   * Gets the Paypal payment creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Paypal payment.
   */
  public function getCreatedTime();

  /**
   * Sets the Paypal payment creation timestamp.
   *
   * @param int $timestamp
   *   The Paypal payment creation timestamp.
   *
   * @return \Drupal\warcraft_core\Entity\PaypalPaymentInterface
   *   The called Paypal payment entity.
   */
  public function setCreatedTime($timestamp);

}
