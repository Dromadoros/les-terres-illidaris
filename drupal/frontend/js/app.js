function toggleBurger() {
  var menus = document.getElementsByClassName('menu');
  var burgerMenu = document.getElementsByClassName('burger-menu');
  var menuMain = document.getElementsByClassName('menu--main');
  for (var i = 0; i < menus.length; i++) {
    if (menus[i].style.display === "block") {
      menus[i].style.display = "none";
      burgerMenu[0].classList.remove('active');
      menuMain[0].classList.remove('open');
      menuMain[0].classList.add('close');
      burgerMenu[0].getElementsByTagName('i')[0].classList.remove('fa-times');
      burgerMenu[0].getElementsByTagName('i')[0].classList.add('fa-ellipsis-v');
    } else {
      menus[i].style.display = "block";
      menuMain[0].classList.remove('close');
      menuMain[0].classList.add('open');
      burgerMenu[0].classList.add('active');
      burgerMenu[0].getElementsByTagName('i')[0].classList.add('fa-times');
      burgerMenu[0].getElementsByTagName('i')[0].classList.remove('fa-ellipsis-v');
    }
  }
}

function copyToClipboard() {
  /* Get the text field */
  var copyText = document.getElementById('realmlist').getElementsByTagName('span')[0];
  var textArea = document.createElement('textarea');
  textArea.value = copyText.innerText;
  document.body.appendChild(textArea);

  /* Select the text field */
  textArea.select();
  textArea.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand('copy');
  document.body.removeChild(textArea);
}

function onClickTab(element) {
  var tabs = document.querySelectorAll('.tab, .tab-title');

  Array.prototype.forEach.call(tabs, function(tab) {
    tab.classList.remove("tab-active");
  });

  element.classList.add("tab-active");
  document.getElementById('tab-' + element.dataset.tabId).classList.add("tab-active");
}

$('.slides').owlCarousel({
  loop:true,
  margin:10,
  nav:false,
  stagePadding: 50,
  responsive:{
    0:{
      items:1
    },
    600:{
      items:2
    },
    1000:{
      items:3
    }
  }
});
