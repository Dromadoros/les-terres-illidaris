import React from "react";
import ReactDOM from "react-dom";
import Countdown from "react-countdown";

var countdown = document.getElementById('countdown-date');

var date = countdown.getAttribute('data-date');

ReactDOM.render(
    <Countdown
        date={date}
        renderer={props => <div className="countdown-elements">
            <span className="countdown-element">{`${props.days} jours`}</span>
            <span className="countdown-element">{`${props.hours} heures`}</span>
            <span className="countdown-element">{`${props.minutes} minutes`}</span>
            <span className="countdown-element">{`${props.seconds} secondes`}</span>
        </div>}
    />,
    countdown
);
