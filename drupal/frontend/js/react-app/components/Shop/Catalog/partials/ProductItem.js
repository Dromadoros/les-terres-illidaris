import React, {Component} from 'react';

class ProductItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showNotConnected: false,
    };
  }

  render() {
    return <button className="product product--item" style={{backgroundImage: `url(${this.props.product.image})`}}
                   onClick={() => this.props.productClick(this.props.product)}>
      <div className="overlay black-gradient"/>

      <h3>{this.props.product.name}</h3>
      <p>{this.props.product.description}</p>
      <span className="price">
        <span>{this.props.product.price}</span> <img className="illidari-mark"
                                                     src="/themes/custom/warcraft/assets/images/illidari_money.jpg"
                                                     alt="Marques d'Illidaris"/>
      </span>
    </button>
  }
}

export default ProductItem;
