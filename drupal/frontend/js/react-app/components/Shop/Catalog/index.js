import React from "react";
import ReactDOM from "react-dom";

import Catalog from "./Catalog";

var shop = document.getElementById('shop');

ReactDOM.render(
  <Catalog
    products={shop.getAttribute('data-products')}
    isConnected={shop.getAttribute('data-is-connected')}
    charactersUrl={shop.getAttribute('data-characters-url')}
    buyItemUrl={shop.getAttribute('data-buy-item-url')}
  />, shop
);
