import React, {Component} from 'react';
import axios from 'axios';
import ProductItem from "./partials/ProductItem";

class Catalog extends React.Component {
    constructor(props) {
        super(props);

        var products = JSON.parse(this.props.products);
        this.state = {
            'products': Object.values(products).map(value => {
                return value;
            }),
            'activeTab': 'all',
            'showBox': false,
            'error': false,
            'errorMessage': '',
            'isLoading': false,
            'characters': {},
            'spe': {},
            'availableSpe': {},
            'boxTitle': '',
            'boxMessage': '',
            'character': false,
            'currentSpe': false,
            'product': false,
            'newCharacterName': false
        };

        this.isActiveTab = this.isActiveTab.bind(this);
        this.setActiveTab = this.setActiveTab.bind(this);
        this.productClick = this.productClick.bind(this);
        this.closePopup = this.closePopup.bind(this);
        this.chooseCharacter = this.chooseCharacter.bind(this);
        this.resetCharacters = this.resetCharacters.bind(this);
        this.onChangeCharacter = this.onChangeCharacter.bind(this);
        this.onChangeSpe = this.onChangeSpe.bind(this);
        this.onCharacterType = this.onCharacterType.bind(this);
    }

    closePopup() {
        this.setState({
            'showBox': false,
            'error': false,
            'errorMessage': '',
            'isLoading': false,
            'characters': {},
            'boxTitle': '',
            'boxMessage': '',
            'character': false,
            'currentSpe': false,
            'spe' : {},
            'availableSpe': {},
            'product': false
        })
    }

    isActiveTab(type) {
        return type === this.state.activeTab;
    }

    setActiveTab(tab) {
        this.setState({
            'activeTab': tab,
        });
    }

    productClick(product) {
        this.setState({
            isLoading: true,
            showBox: true,
            product: product,
        });

        axios.get(this.props.charactersUrl)
            .then(res => {
                this.setState({
                    isLoading: false,
                });

                if (res.status !== 200 || res.data.error) {
                    this.setState({
                        error: true,
                        errorMessage: res.data.message,
                        boxTitle: res.data.title
                    });

                    return;
                }

                this.setState({
                    error: false,
                    characters: res.data.characters,
                    spe: res.data.spe,
                    boxTitle: res.data.title
                });
            }).catch(error => {
            this.setState({
                error: true,
                errorMessage: res.data.message,
                boxTitle: res.data.title
            });
        });
    }

    onChangeCharacter(e) {
        const characterId = e.target.value;
        this.setState({
            character: e.target.value,
            availableSpe: characterId ? this.state.spe[this.state.characters[characterId]['class']] : false
        });
    }

    onChangeSpe(e) {
        this.setState({currentSpe: e.target.value});
    }

    onCharacterType(e) {
        this.setState({newCharacterName: e.target.value});
    }

    resetCharacters() {
        this.setState({
            characters: {},
            character: false
        });
    }

    chooseCharacter() {
        if (!this.state.character) {
            this.setState({
                error: true,
                errorMessage: window.Drupal.t('Please choose a character'),
            });

            return;
        }

        if (this.state.product.serviceType == 'boost' && !this.state.currentSpe) {
            this.setState({
                error: true,
                errorMessage: window.Drupal.t('Please choose a spe'),
            });

            return;
        }

        this.setState({
            isLoading: true,
            showBox: true,
            error: false,
            errorMessage: '',
            boxMessage: '',
        });

        axios.post(this.props.buyItemUrl, {
            'character': this.state.character,
            'productId': this.state.product.id,
            'newCharacterName': this.state.newCharacterName,
            'spe': this.state.currentSpe
        }).then(res => {
            this.setState({
                isLoading: false
            });
            if (res.data.error) {
                this.setState({
                    error: true,
                    errorMessage: res.data.message,
                });

                return;
            }

            this.resetCharacters();

            this.setState({
                boxTitle: res.data.title,
                boxMessage: res.data.message,
            })
        }).catch(error => {
            console.log(error);
        })
    }

    render() {
        return <div className="catalog">
            {this.state.isLoading ? <div className="lds-dual-ring"/> : ''}
            <div className="wrapper">
                <div className="tabs">
                    {this.state.products.map((value) =>
                        <button className={`tab tab--items ${this.isActiveTab(value.id) ? 'is-active' : ''}`}
                                onClick={() => this.setActiveTab(value.id)}>{value.name}</button>
                    )}
                </div>
                {this.state.showBox && <div className="box-not-connected">
                    <div className="overlay black-opacity"/>
                    <div className="container">
                        <span className="close" onClick={this.closePopup}><i className="fas fa-times"/></span>
                        {this.state.boxTitle && <h2>{this.state.boxTitle}</h2>}
                        {this.state.boxMessage && <p dangerouslySetInnerHTML={{__html: this.state.boxMessage}}/>}
                        {this.state.error &&
                        <p className="error" dangerouslySetInnerHTML={{__html: this.state.errorMessage}}/>}
                        {Object.values(this.state.characters).length != 0 && <div>
                            <p
                                className="item-text"
                                dangerouslySetInnerHTML={{__html: window.Drupal.t('Which character do you want to receive this item : <span>@item</span> ?', {'@item': this.state.product.name})}}/>
                            <select onChange={this.onChangeCharacter} name="characters">
                                <option key="default" value="">{window.Drupal.t('Your character')}</option>
                                {Object.values(this.state.characters).map(character => {
                                    return <option key={character.id} value={character.id}>{character.name}</option>;
                                })}
                            </select>
                            {this.state.character && this.state.product.serviceType == 'boost' &&
                            <select required onChange={this.onChangeSpe} name="spe">
                                <option key="default_spe" value="">{window.Drupal.t('Votre spécialisation')}</option>
                                {Object.entries(this.state.availableSpe).map((value) => {
                                    return <option key={value[0]} value={value[0]}>{value[1].label}</option>;
                                })}
                            </select>}
                            <button onClick={this.chooseCharacter}>{window.Drupal.t('Confirm buy')} <span
                                className="price"> {this.state.product.price} <img
                                className="gold-icon illidari-mark"
                                src="/themes/custom/warcraft/assets/images/illidari_money.jpg"
                                alt="Marques d'Illidaris"/></span></button>
                        </div>
                        }
                    </div>
                </div>}

                <div className="tabs-content">
                    {this.state.products.map((value) =>
                        <div
                            className={`tab-content tab-content--items ${this.isActiveTab(value.id) ? 'is-active' : ''}`}>
                            {Object.values(value.products).map(item =>
                                <ProductItem
                                    key={item.id}
                                    product={item}
                                    isConnected={this.props.isConnected}
                                    loginUrl={this.props.loginUrl}
                                    productClick={this.productClick}
                                />
                            )}
                        </div>
                    )}
                </div>
            </div>
        </div>
    }
}

export default Catalog;
