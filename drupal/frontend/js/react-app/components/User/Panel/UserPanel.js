import React, {Component} from 'react';
import PasswordValidator from 'password-validator';
import axios from 'axios';

class UserPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      repeatPassword: '',
      passwordValid: true,
      matchingPassword: true,
      updated: false,
      validPassword: true,
      isLoading: false,
    };

    this.schema = new PasswordValidator();
    this.schema
      .is().min(6)
      .is().max(50)
      .has().not().spaces();
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordRepeatChange = this.handlePasswordRepeatChange.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
  }

  handlePasswordChange(e) {
    var value = e.target.value;
    var isMatchingPassword = value === this.state.repeatPassword;

    this.setState({
      password: value,
      matchingPassword: isMatchingPassword,
      validPassword: true
    });
  }

  handlePasswordRepeatChange(e) {
    e.preventDefault();
    var value = e.target.value;
    var isMatchingPassword = value === this.state.password;

    this.setState({
      repeatPassword: value,
      matchingPassword: isMatchingPassword,
      validPassword: true,
    });
  }

  validatePassword(e) {
    this.setState({
      isLoading: true,
    });

    if (!this.state.matchingPassword) {
      this.setState({
        isLoading: false,
      });

      return;
    }

    var isPasswordValid = this.schema.validate(this.state.password);

    this.setState({
      validPassword: isPasswordValid,
    });

    if (!isPasswordValid) {
      this.setState({
        isLoading: false,
      });
      return;
    }

    axios.post(this.props.apiUrl, {'newPassword': this.state.password})
      .then(res => {
        this.setState({
          isLoading: false,
          updated: true
        });
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    if (this.state.updated) {
      return <div className="form reset-password">
        <div className="overlay"></div>
        <div className="wrapper">
          <div className="create-form">
            <h2><i className="fas fa-user-circle"></i> {window.Drupal.t('Update my password')}</h2>
            <p><i className="far fa-check-circle"></i> {window.Drupal.t('Mot de passe modifié avec succès.')}</p>
          </div>
        </div>
      </div>
    }
    return (
      <div className="form reset-password">
        <div className="overlay"></div>
        <div className="wrapper">
          <div className="create-form">
            <h2><i className="fas fa-user-circle"></i>{window.Drupal.t('Modifier son mot de passe')}</h2>
            {this.state.isLoading ? <div className="lds-dual-ring"/> : ''}
            <form className={"account-form " + (this.state.isLoading ? 'is-loading' : '')}>
              <label>
                {window.Drupal.t('Email')}:
                <input type="text" name="email" value={this.props.email} disabled={true}/>
              </label>
              <label>
                {window.Drupal.t('Password')}:
                <input onChange={this.handlePasswordChange} type="password" name="password[pass1]"
                       value={this.state.password}/>
                {this.state.matchingPassword ? '' :
                  <span className="password-matching-error error">{window.Drupal.t('Password does\'nt match')}</span>}
                {this.state.validPassword ? '' :
                  <span
                    className="password-valid-error error">{window.Drupal.t('Password is not valid, please minimum 6 characters, maximum 50 characters and no spaces')}</span>}
              </label>
              <label>
                {window.Drupal.t('Repeat password')}:
                <input onChange={this.handlePasswordRepeatChange} type="password" name="password[pass2]"
                       value={this.state.repeatPassword}/>
              </label>
              <input type="button" disabled={this.state.isLoading} onClick={this.validatePassword}
                     value={window.Drupal.t('Modify')}/>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default UserPanel
