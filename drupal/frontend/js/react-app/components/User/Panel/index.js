import React from "react";
import ReactDOM from "react-dom";

import UserPanel from './UserPanel';

var userPanel = document.getElementById('user-panel');


ReactDOM.render(
  <UserPanel
    email={userPanel.getAttribute('data-user-email')}
    username={userPanel.getAttribute('data-user-username')}
    points={userPanel.getAttribute('data-user-points')}
    apiUrl={userPanel.getAttribute('data-user-api-url')}
  />,
  document.getElementById('user-panel')
);
