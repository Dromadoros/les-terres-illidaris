import React, {Component} from 'react';

class Viewer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isView: false,
    };

    this.clickView = this.clickView.bind(this);
  }

  clickView(e) {
    this.setState({
      isView: true,
    });

    var items = this.props.items;
    var skinInfo = JSON.parse(this.props.skinInfo);
    items = JSON.parse(this.props.items);
    items = Object.keys(items).map(function (key, index) {
      return [
        parseInt(items[key].position), items[key].displayId
      ]
    });

    var model = {
      type: ModelViewer.WOW,
      contentPath: "https://legion-viewer.firestorm-servers.com/",
      container: jQuery('.model'),
      hd: true,
      aspect: 0.76,
      sk: parseInt(skinInfo.skin),
      ha: parseInt(skinInfo.hair),
      hc: parseInt(skinInfo.hairColor),
      fa: parseInt(skinInfo.face),
      fh: parseInt(skinInfo.facial),
      fc: 0,
      ep: 0,
      ho: 0,
      ta: 0,
      cls: parseInt(skinInfo.class),
      items: items,
      models: {
        type: ModelViewer.Wow.Types.CHARACTER,
        id: `${ModelViewer.Wow.Races[skinInfo.race]}${ModelViewer.Wow.Genders[skinInfo.gender]}`
      }
    };

    var viewer = new ModelViewer(model);
  }

  render() {
    return <div className="model">
      {this.state.isView ? '' :
        <input class="button-viewer" type="button" value={window.Drupal.t('View 3D')} onClick={this.clickView}/>
        }
        <div className="canvas-div"></div>
    </div>
  }
}

export default Viewer
