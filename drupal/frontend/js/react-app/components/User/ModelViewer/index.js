import React from "react";
import ReactDOM from "react-dom";

import Viewer from './Viewer';

var model = document.getElementById('model-wrapper');

ReactDOM.render(
  <Viewer
    items={model.getAttribute('data-items')}
    skinInfo={model.getAttribute('data-skin-info')}
  />,
  model
);
