import React, {Component} from 'react';
import axios from 'axios';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isLoading: false,
      error: false,
      success: false
    };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.login = this.login.bind(this);
  }

  handleEmailChange(e) {
    var value = e.target.value;

    this.setState({
      email: value,
      error: false
    });
  }

  handlePasswordChange(e) {
    var value = e.target.value;

    this.setState({
      password: value,
      error: false
    });
  }

  login(e) {
    this.setState({
      isLoading: true,
    });

    axios.post(this.props.loginApiUrl, {
      'email': this.state.email,
      'password': this.state.password
    })
      .then(res => {
        this.setState({
          isLoading: false,
        });

        if (res.status !== 200) {
          this.setState({
            error: true,
          });

          return;
        }

        this.setState({
          success: true,
        });

        window.location.replace(res.data.url);
      }).catch(error => {
      this.setState({
        error: true,
        isLoading: false
      });
    });
  }

  render() {
    if (this.state.success) {
      return <div className="form login">
        <div className="overlay"></div>
        <div className="wrapper">
          <div className="create-form">
            <h2><i className="fas fa-user-circle"></i> {window.Drupal.t('Login')}</h2>
            <p><i className="far fa-check-circle"></i> {window.Drupal.t('Vous êtes à présent connecté(e), vous allez être redirigé(e).')}</p>
          </div>
        </div>
      </div>
    }
    return (
      <div className="form login">
        <div className="overlay"></div>
        <div className="wrapper">
          <div className="create-form">
            <h2><i className="fas fa-user-circle"></i>{window.Drupal.t('Login')}</h2>
            {this.state.isLoading ? <div className="lds-dual-ring"/> : ''}
            <form className={"account-form"}>
              <label>
                {window.Drupal.t('E-mail')}:
                <input onChange={this.handleEmailChange}  type="email" name="email" value={this.state.email}/>
                {!this.state.error ? '' :
                  <span className="error">{window.Drupal.t('Email or password is not correct')}</span>}
              </label>
              <label>
                {window.Drupal.t('Password')}:
                <input onChange={this.handlePasswordChange} type="password" name="password"
                       value={this.state.password}/>
              </label>
              <input disabled={this.state.isLoading} type="button" onClick={this.login}
                     value={window.Drupal.t('Login')}/>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login
