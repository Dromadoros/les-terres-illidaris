import React from "react";
import ReactDOM from "react-dom";

import Login from "../Login/Login";

var userLogin = document.getElementById('user-login');

ReactDOM.render(
  <Login loginApiUrl={userLogin.getAttribute('data-api-login')} />,
  userLogin
);
