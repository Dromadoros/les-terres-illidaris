const mix = require('laravel-mix');

mix.react('frontend/js/react-app/components/User/Panel/index.js', './web/themes/custom/warcraft/assets/js/user/panel');
mix.react('frontend/js/react-app/components/User/Login/index.js', './web/themes/custom/warcraft/assets/js/user/login');
mix.react('frontend/js/react-app/components/User/ModelViewer/index.js', './web/themes/custom/warcraft/assets/js/user/modelViewer');
mix.react('frontend/js/react-app/components/Shop/Catalog/index.js', './web/themes/custom/warcraft/assets/js/shop/catalog');
mix.react('frontend/js/react-app/components/Countdown/index.js', './web/themes/custom/warcraft/assets/js/countdown');
