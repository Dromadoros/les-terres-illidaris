<?php
require_once('config/config.php');

function secondsToTime($seconds) {
  $dtF = new \DateTime('@0');
  $dtT = new \DateTime("@$seconds");

  return $dtF->diff($dtT)->format('%aj %hh %im %ss');
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset('utf8');
// Create connection
$realmdConn = new mysqli($servername, $username, $password, $dbrealmdname);
$realmdConn->set_charset('utf8');
// Check connection
if ($conn->connect_error || $realmdConn->connect_error) {
  die("Problème de connexion ...");
}

date_default_timezone_set('Europe/Brussels');

$queryMaxLevel = "SELECT guid, name, totaltime, leveltime, totaltime - leveltime as maxleveltime, level, race, class, dingTime FROM characters WHERE level = 70 AND dingTime !=0 ORDER BY maxleveltime ASC";
$queryFirsts = "SELECT guid, name, level, race, class, dingTime FROM characters WHERE level = 70 AND dingTime !=0 ORDER BY dingTime ASC";
$queryStillLevelings = "SELECT guid, name, level, race, class FROM characters WHERE level != 70 AND isDead =0 ORDER BY level DESC";
$queryOnlinePlayers = "SELECT guid, name, level, race, class, online FROM characters WHERE online = 1";
$resultMaxLevel = $conn->query($queryMaxLevel);
$resultFirsts = $conn->query($queryFirsts);
$resultStillLevelings = $conn->query($queryStillLevelings);
$resultOnlinePlayers = $conn->query($queryOnlinePlayers);

$survivals = [];
$firsts = [];
$onlines = [];
$stillLevelings = [];

if ($resultMaxLevel->num_rows > 0) {
  // output data of each row
  while ($row = $resultMaxLevel->fetch_assoc()) {
    $survivals[] = $row;
  }

  $survivals = array_slice($survivals, 0, 30);
}
if ($resultFirsts->num_rows > 0) {
  // output data of each row
  while ($row = $resultFirsts->fetch_assoc()) {
    $firsts[] = $row;
  }

  $firsts = array_slice($firsts, 0, 10);
}
if ($resultStillLevelings->num_rows > 0) {
  // output data of each row
  while ($row = $resultStillLevelings->fetch_assoc()) {
    $stillLevelings[] = $row;
  }

  $stillLevelings = array_slice($stillLevelings, 0, 15);
}

if ($resultOnlinePlayers->num_rows > 0) {
  // output data of each row
  $resultOnlinePlayers->field_count;
  while ($row = $resultOnlinePlayers->fetch_assoc()) {
    $onlines[] = $row;
  }
}
$conn->close();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Hardcore - Les Terres Illidaris</title>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description"
          content="Un sous-royaume du serveur privé Les Terres Illidaris proposant un mode dit hardcore. Vous mourrez, vous recommencez. Arrivé 70, recevez une énorme récompense."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords"
          content="serveur privé, burning-crusade, bc, 2.4.3, world of warcraft, wow, les terres illidaris, "/>
    <meta name="author" content="Les Terres Illidaris"/>
    <meta name="copyright" content="Les Terres Illidaris"/>
    <script>const whTooltips = {
            colorLinks: true,
            iconizeLinks: false,
            renameLinks: false
        };</script>
    <script src="https://wow.zamimg.com/widgets/power.js"></script>
    <script src="https://kit.fontawesome.com/85a2340774.js"
            crossorigin="anonymous"></script>
    <link href="assets/css/style.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap"
          rel="stylesheet">
</head>
<body>
<div class="overlay-body"></div>
<header>
<!--    <div class="online">--><?php //echo count($onlines); ?><!--</div>-->
    <div class="video">
        <video autoplay loop muted preload="metadata">
            <source src="/assets/video/illidan_animated.mp4"
                    type="video/mp4">
        </video>
    </div>
    <img class="logo" src="assets/images/logo_hardcore_render.png"
         alt="Les Terres Illidaris - Hardcore"/>
    <div class="text">
        <h1 data-text="Les Terres Illidaris">Les Terres Illidaris</h1>
        <h2 data-text="Hardcore mode">Hardcore mode</h2>
        <p>Vous n'avez qu'une vie aventurier. Mourrez et c'est le <b>retour
                à la
                case de départ</b>. Parcourez à nouveau le monde d'Azeroth
            mais
            cette fois-ci ... Soyez encore plus concentré. Chaque combat
            compte,
            du Kobold dans les
            Forêt d'Elwynn jusqu'à l'Inferno en Outre-Terre. Réussissez et
            recevez une grande récompense.
        <br />
        <br />
            <b>Le mode hardcore est actuellement fermé. Nous nous reverrons à la prochaine saison.</b>
        </p>
        <a class="text-gold text-shadow subscription"
           href="https://les-terres-illidaris.fr/#subscription-form">S'inscrire
            <i class="fas fa-external-link-alt text-gold"></i>
        </a>
    </div>

    <div class="border-bg"></div>
    <div class="border-bg-bottom"></div>
</header>

<div class="wrapper">
    <div class="gifts">
        <div class="text">
            <div class="border-gift"></div>
            <h2 class="text-gold text-shadow">Les récompenses</h2>

            <ul>
                <li class="gift" style="color: #ff8000;">
                    <div>
                        <div class="gift-border transfert"></div>
                        <p class="text-shadow">Un personnage équipé niveau 70 sur le royaume <a
                                    class="text-gold"
                                    href="https://les-terres-illidaris.fr/"
                                    target="_blank"> <b>Les Terres
                                    Illidaris</b></a></p></div>
                </li>
                <li class="gift">
                    <div>
                        <div class="gift-border belier"></div>
                        <p class="text-shadow"><a href="#"
                                                  data-wowhead="item=33977&amp;domain=fr">Une
                                monture : Bélier rapide de la fête des
                                Brasseurs</a></p></div>
                </li>
                <li class="gift">
                    <div>
                        <div class="gift-border zergling"></div>
                        <p class="text-shadow"><a href="#"
                                                  data-wowhead="item=13582&amp;domain=fr">Une
                                mascotte super sympathique</a></p></div>
                </li>
                <li class="gift">
                    <div>
                        <div class="gift-border medaillon"></div>
                        <p class="text-shadow"><a href="#"
                                                  data-wowhead="item=28240&amp;domain=fr">Un
                                médaillon de la horde/alliance</a></p></div>
                </li>
                <li class="gift" style="color: #ff8000;">
                    <div>
                        <div class="gift-border gold"></div>
                        <p class="text-shadow">1.000 po</p></div>
                </li>
                <li class="gift" style="color: #ff8000;">
                    <div>
                        <div class="gift-border classement"></div>
                        <p class="text-shadow">Votre nom dans le
                            classement ci-dessous
                        </p></div>
                </li>
            </ul>

            <p class="text-shadow">Ces récompenses peuvent être récupérées à partir du <a
                        class="text-gold"
                        href="https://les-terres-illidaris.fr">site web
                    principal</a>. Connectez-vous, cliquez en haut à droite sur
                votre nom de compte, vous serez dans votre espace utilisateur.
                Scrollez plus bas dans la page ou vous aurez la possibilité de recevoir votre personnage et récompenses</p>
        </div>
    </div>
</div>
<div class="border">
    <div class="border-bg-bottom"></div>
</div>

<div class="bg firsts">
    <div class="wrapper" style="background: none; box-shadow: none">
        <div class="ranking">
            <h2 class="text-gold text-shadow">Les premiers</h2>
          <?php if (empty($firsts)) {
            echo '<p class="text-shadow">Pas encore de survivants ...</p>';
          }
          else {
            echo '<ul>';
            foreach ($firsts as $first) {
              echo '<li><img src="/assets/images/class/'. $first['class'] .'.gif" alt="first" /><span class="text-shadow text-gold">' . $first['name'] . '</span> ' .
                date('d/m/Y - H:i', $first['dingTime'])
                . '</li>';
            }
            echo '</ul>';
          } ?>
        </div>
    </div>
</div>

<div class="border">
    <div class="border-bg-bottom"></div>
</div>

<div class="bg">
    <div class="wrapper" style="background: none; box-shadow: none">
        <div class="ranking">
            <h2 class="text-gold text-shadow">La course au /played</h2>
          <?php if (empty($survivals)) {
            echo '<p class="text-shadow">Pas encore de survivants ...</p>';
          }
          else {
            echo '<ul>';
            foreach ($survivals as $survival) {
              echo '<li><img src="/assets/images/class/'. $survival['class'] .'.gif" alt="survival "/><span class="text-shadow text-gold">' . $survival['name'] . '</span> ' . secondsToTime(
                  $survival['maxleveltime']
                ) . '</li>';
            }
            echo '</ul>';
          } ?>
        </div>
    </div>
</div>

<div class="border">
    <div class="border-bg-bottom"></div>
</div>

<div class="bg leveling" style="background-image: url('/assets/images/marche_ouest.jpg');">
    <div class="wrapper" style="background: none; box-shadow: none">
        <div class="ranking">
            <h2 class="text-gold text-shadow">Les joueurs proches de la victoire et encore vivants</h2>
          <?php if (empty($stillLevelings)) {
            echo '<p class="text-shadow">Pas de joueurs ...</p>';
          }
          else {
            echo '<ul>';
            foreach ($stillLevelings as $stillLeveling) {
              echo '<li><img src="/assets/images/class/'. $stillLeveling['class'] .'.gif" alt="leveling "/><span class="text-shadow text-gold">' . $stillLeveling['name'] . '</span> ' . $stillLeveling['level'] . '</li>';
            }
            echo '</ul>';
          } ?>
        </div>
    </div>
</div>

<div class="border">
    <div class="border-bg-bottom"></div>
</div>

<div class="server-info">
    <div class="wrapper">
        <h2 class="text-shadow text-gold">Configuration du royaume</h2>
        <table style="width:50%">
            <tr>
                <td>Server.PVP</td>
                <td>OFF</td>
            </tr>
            <tr>
                <td>Server.Interfaction</td>
                <td>ON</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Poor</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Normal</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Uncommon</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Rare</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Epic</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Legendary</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Artifact</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Referenced</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Item.Quest</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Drop.Money</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Rate.Pet.XP.Kill</td>
                <td>8</td>
            </tr>
            <tr>
                <td>Rate.XP.Kill</td>
                <td>8</td>
            </tr>
            <tr>
                <td>Rate.XP.Quest</td>
                <td>8</td>
            </tr>
            <tr>
                <td>Rate.XP.Explore</td>
                <td>8</td>
            </tr>
            <tr>
                <td>Rate.Rest.InGame</td>
                <td>1</td>
            </tr>

        </table>
    </div>
</div>

<div class="border">
    <div class="border-bg-bottom"></div>
</div>

<div class="footer">
    <p class="text-shadow">Visitez aussi notre royaume principal
        <a class="text-gold" target="_blank"
           href="https://les-terres-illidaris.fr">
            Les Terres Illidaris
        </a>
    </p>
    <p class="copyright">© 2020 - Les Terres Illidaris tout droit réservé</p>
</div>

</body>
</html>
