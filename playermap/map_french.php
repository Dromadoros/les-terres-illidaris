<?php

$character_race = [
  1 => 'Humain',
  2 => 'Orc',
  3 => 'Nain',
  4 => 'Elf de la Nuit',
  5 => 'Mort-vivant',
  6 => 'Tauren',
  7 => 'Gnome',
  8 => 'Troll',
  9 => 'Goblin',
  10 => 'Elf de Sang',
  11 => 'Draeneï',
];

$character_class = [
  1 => 'Guerrier',
  2 => 'Paladin',
  3 => 'Chasseur',
  4 => 'Voleur',
  5 => 'Prêtre',
  6 => 'Chevalier de la mort',
  7 => 'Chaman',
  8 => 'Mage',
  9 => 'Démoniste',
  11 => 'Druide',
];

$lang_defs = [
  'maps_names' => ['Azeroth', 'Outreterre'],
  'total' => 'Total',
  'faction' => ['Alliance', 'Horde'],
  'name' => 'Nom',
  'race' => 'Race',
  'class' => 'Classe',
  'level' => 'lvl',
  'click_to_next' => 'Click: go to next',
  'click_to_first' => 'Click: go to first',
];

include "zone_names_french.php";
?>
